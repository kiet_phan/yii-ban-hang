<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use faryshta\base\EnumTrait;
use yii\db\Expression;
use faryshta\validators\EnumValidator;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customers".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $password
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $province
 * @property string $district
 * @property string $first_name
 * @property string $last_name
 * @property string $ship_address
 *
 * @property Transactions[] $transactions
 */
class Customers extends \yii\db\ActiveRecord implements IdentityInterface
{
  /**
   * @inheritdoc
   */
  public $password_confirm;
  public static function tableName()
  {
    return 'customers';
  }
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }
  public function fields()
  {
    $fields=  parent::fields(); // TODO: Change the autogenerated stub
    unset($fields['password']);
    return $fields;

  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['first_name', 'last_name', 'email', 'password','password_confirm','address','province'], 'required'],
      [[ 'email', 'address','ship_address'], 'string', 'max' => 255],
      [[ 'password', 'province', 'district', 'first_name', 'last_name'], 'string', 'max' => 50],
      ['password_confirm', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
      [['phone'], 'string', 'max' => 20],
      [['email'], 'unique'],
      ['email', 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'email' => 'Email',
      'phone' => 'Phone',
      'address' => 'Address',
      'password' => 'Password',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'province' => 'Province',
      'district' => 'District',
      'first_name' => 'Tên',
      'last_name' => 'Họ',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTransactions()
  {
    return $this->hasMany(Transactions::className(), ['customer_id' => 'id']);
  }
  public function beforeSave($insert)
  {
    $this->setPassword($this->password);

    return parent::beforeSave($insert);
  }

  public function setPassword($password)
  {
    $this->password = Yii::$app->security->generatePasswordHash($password);
  }
  public static function findIdentity($id)
  {

    return static::findOne(['id' => $id]);

    // TODO: Implement findIdentity() method.
  }

  public static function findIdentityByAccessToken($token, $type = null)
  {
    // TODO: Implement findIdentityByAccessToken() method.
  }

  public function getId()
  {

    return $this->getPrimaryKey();
  }

  public function getAuthKey()
  {
    // TODO: Implement getAuthKey() method.
  }

  public function validateAuthKey($authKey)
  {
    // TODO: Implement validateAuthKey() method.
  }
  public static function findByEmail($email)
  {
    return static::findOne(['email' => $email]);

  }
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password);
  }

}
