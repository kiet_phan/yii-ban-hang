<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use faryshta\base\EnumTrait;
use yii\db\Expression;
use faryshta\validators\EnumValidator;
/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_phone
 * @property string $address
 * @property string $amount
 * @property string $payment
 * @property string $payment_info
 * @property integer $status
 * @property string $security
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $province
 * @property string $district
 * @property string $first_name
 * @property string $last_name
 *
 * @property Orders[] $orders
 * @property Orders[] $orders0
 * @property Customers $customer
 */
class Transactions extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  use EnumTrait;
  public static function tableName()
  {
    return 'transactions';
  }
  public function beforeSave($insert)
  {
    if($insert){
      $this->order_num = $this->getUniqueNumber();
      $this->status = 0;
    }
    return parent::beforeSave($insert); // TODO: Change the autogenerated stub
  }

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }


  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['customer_id', 'created_at', 'updated_at'], 'integer'],
      ['user_email', 'email'],
      [['user_email', 'user_phone', 'address', 'province', 'district', 'first_name', 'last_name'], 'required'],
      [['amount'], 'number'],
      [['payment_info'], 'string'],
      [[ 'user_email', 'user_phone', 'address', 'province', 'district', 'first_name', 'last_name'], 'string', 'max' => 255],
      [['payment', 'security'], 'string', 'max' => 40],
      [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
    ];
  }
  public static function enums()
  {
    return [
      // this is the name of the enum.
      'status' => [
        // here it follows the `'index' => 'desc'` notation
        '0' => 'New',
        '1' => 'Process',
        '2' => 'Done',
        '3' => 'Cancel',
      ]
    ];
  }
  public function getStatusDesc()
  {
    // method provided in the EnumTrait to get the description of the value
    // of the attribute
    return $this->getAttributeDesc('status');
  }


  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'customer_id' => 'Customer ID',
      'user_email' => ' Email',
      'user_phone' => 'Phone',
      'address' => 'Address',
      'amount' => 'Amount',
      'payment' => 'Payment',
      'payment_info' => 'Payment Info',
      'status' => 'Status',
      'security' => 'Security',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'province' => 'Province',
      'district' => 'District',
      'first_name' => 'First Name',
      'last_name' => 'Last Name',
    ];
  }
  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOrders()
  {
    return $this->hasMany(Orders::className(), ['transaction_id' => 'id']);
  }
  public function getUniqueNumber(){
    $num_str = "";
    $exits =true;
    while($exits){
      $num_str = sprintf("%08d", mt_rand(1, 999999));
      $orderExist =$this->findOne(['order_num'=>$num_str]);
      if(!$orderExist){
        $exits = false;
      }
    }
    return $num_str;

  }

  /**
   * @return \yii\db\ActiveQuery
   */

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCustomer()
  {
    return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
  }
  public function getName(){
    return $this->last_name.' '.$this->first_name;
  }
}
