<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Employees]].
 *
 * @see Employees
 */

use creocoder\nestedsets\NestedSetsQueryBehavior;

class CategoriesQuery extends \yii\db\ActiveQuery
{
  /*public function active()
  {
      $this->andWhere('[[status]]=1');
      return $this;
  }*/

  /**
   * @inheritdoc
   * @return Employees[]|array
   */
  public function behaviors()
  {
    return [
      NestedSetsQueryBehavior::className(),
    ];
  }
  public function all($db = null)
  {
    return parent::all($db);
  }

  /**
   * @inheritdoc
   * @return Employees|array|null
   */
  public function one($db = null)
  {
    return parent::one($db);
  }
}