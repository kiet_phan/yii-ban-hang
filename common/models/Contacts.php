<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $comment
 */
class Contacts extends \yii\db\ActiveRecord
{
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'contacts';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['comment'], 'string'],
      [['name', 'email', 'phone', 'comment'], 'required'],
      ['email', 'email'],
      [['name', 'email', 'phone'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'email' => 'Email',
      'phone' => 'Phone',
      'comment' => 'Comment',
    ];
  }
}
