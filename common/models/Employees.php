<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use Yii;
use faryshta\base\EnumTrait;
use yii\db\Expression;
use faryshta\validators\EnumValidator;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "employees".
 *
 * @property integer $id
 * @property string $name
 * @property string $auth_key
 * @property string $password
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Employees extends \yii\db\ActiveRecord implements IdentityInterface
{
  /**
   * @inheritdoc
   */
  use EnumTrait;
  const STATUS_DELETED = 0;
  const STATUS_ACTIVE = 1;

  public static function tableName()
  {
    return 'employees';
  }

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ]
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['role'], EnumValidator::className()],
      [['name', 'password', 'email'], 'required'],
      [['role', 'status'], 'integer'],
      [['name', 'password', 'email'], 'string', 'max' => 255],
      [['auth_key'], 'string', 'max' => 32],
      [['email'], 'unique'],
      [['password_reset_token'], 'unique']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Tên'),
      'auth_key' => Yii::t('app', 'Auth Key'),
      'password' => Yii::t('app', 'Password'),
      'password_reset_token' => Yii::t('app', 'Password Reset Token'),
      'email' => Yii::t('app', 'Email'),
      'role' => Yii::t('app', 'Quyền '),
      'status' => Yii::t('app', 'Trạng Thái'),
      'created_at' => Yii::t('app', 'Ngày tạo '),
      'updated_at' => Yii::t('app', 'Ngày chỉnh sửa'),
    ];
  }

  /**
   * @inheritdoc
   * @return EmployeesQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new EmployeesQuery(get_called_class());
  }

  public function beforeSave($insert)
  {
    $this->setPassword($this->password);

    return parent::beforeSave($insert);
  }

  public function setPassword($password)
  {
    $this->password = Yii::$app->security->generatePasswordHash($password);
  }

  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }


  public static function enums()
  {
    return [
      // this is the name of the enum.
      'status' => [
        // here it follows the `'index' => 'desc'` notation
        '0' => 'Inactive',
        '1' => 'Active',
      ],
      'role' => [
        '0' => 'Supper Admin',
        '1' => 'Employess'
      ]
    ];
  }

  // optional magic method to access the value quickly
  public function getStatusDesc()
  {
    // method provided in the EnumTrait to get the description of the value
    // of the attribute
    return $this->getAttributeDesc('status');
  }

  public function getRoleDesc()
  {
    // method provided in the EnumTrait to get the description of the value
    // of the attribute
    return $this->getAttributeDesc('role');
  }


  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken()
  {
    $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken()
  {
    $this->password_reset_token = null;
  }

  /**
   * Finds an identity by the given ID.
   * @param string|integer $id the ID to be looked for
   * @return IdentityInterface the identity object that matches the given ID.
   * Null should be returned if such an identity cannot be found
   * or the identity is not in an active state (disabled, deleted, etc.)
   */
  public static function findIdentity($id)
  {
    return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  /**
   * Returns an ID that can uniquely identify a user identity.
   * @return string|integer an ID that uniquely identifies a user identity.
   */
  public function getId()
  {

    return $this->getPrimaryKey();
  }

  /**
   * Returns a key that can be used to check the validity of a given identity ID.
   *
   * The key should be unique for each individual user, and should be persistent
   * so that it can be used to check the validity of the user identity.
   *
   * The space of such keys should be big enough to defeat potential identity attacks.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @return string a key that is used to check the validity of a given identity ID.
   * @see validateAuthKey()
   */
  public function getAuthKey()
  {
    return $this->id;

    // TODO: Implement getAuthKey() method.
  }

  /**
   * Validates the given auth key.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @param string $authKey the given auth key
   * @return boolean whether the given auth key is valid.
   * @see getAuthKey()
   */
  public function validateAuthKey($authKey)
  {
    return $this->auth_key;
    // TODO: Implement validateAuthKey() method.
  }

  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password);
  }

  public static function findByEmail($email)
  {
    return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);

  }

}
