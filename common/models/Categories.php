<?php

namespace common\models;

use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $depth
 * @property integer $tree
 * @property integer $lft
 * @property integer $parent_id
 * @property integer $rgt
 * @property integer $created_at
 * @property integer $updated_at
 */
class Categories extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'categories';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['depth', 'tree','parent_id', 'lft', 'rgt', 'created_at', 'updated_at'], 'integer'],
      [['name'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Name'),
      'depth' => Yii::t('app', 'Depth'),
      'parent_id' => Yii::t('app', 'Danh Muc Cha'),
      'tree' => Yii::t('app', 'Tree'),
      'lft' => Yii::t('app', 'Lft'),
      'rgt' => Yii::t('app', 'Rgt'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  public function behaviors()
  {
    return [
      'tree' => [
        'class' => NestedSetsBehavior::className(),
         'treeAttribute' => 'tree',
        // 'leftAttribute' => 'lft',
        // 'rightAttribute' => 'rgt',
        // 'depthAttribute' => 'depth',
      ],
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ],
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::className(),
        'relations' => ['images']
      ],
      [
        'class' => SluggableBehavior::className(),
        'attribute' => 'name',
        // 'slugAttribute' => 'slug',
      ]
    ];
  }

  public function transactions()
  {
    return [
      self::SCENARIO_DEFAULT => self::OP_ALL,
    ];
  }
  public function getProducts(){
    return $this->hasMany(Products::className(),['category_id'=> "id"]);
  }

  public static function find()
  {
    return new CategoriesQuery(get_called_class());
  }
}
