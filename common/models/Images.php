<?php

namespace common\models;

use backend\models\behavior\UploadBehavior;
use Yii;
use mongosoft\file\UploadImageBehavior;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $file_path
 * @property string $type
 * @property integer $size
 * @property integer $product_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Images extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'images';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['size', 'product_id', 'created_at', 'updated_at'], 'integer'],
      ['file_path', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
      [['file_name', 'type'], 'string', 'max' => 255],
      [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'file_name' => Yii::t('app', 'File Name'),
      'file_path' => Yii::t('app', 'File Path'),
      'type' => Yii::t('app', 'Type'),
      'size' => Yii::t('app', 'Size'),
      'product_id' => Yii::t('app', 'Product ID'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }
  public function behaviors()
  {
    return [
      [
        'class' => UploadBehavior::className(),
        'thumbs' => [
          'thumb' => ['width' => 700, 'height' => 850, 'quality' => 90],
          'preview' => ['width' => 200, 'height' => 250],
        ],
      ],
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at'
      ],
      'timestamp' => [
        'class' => 'yii\behaviors\TimestampBehavior',
        'attributes' => [
          \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
      ],
    ];
  }

}
