<?php
/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 5/4/16
 * Time: 12:34 AM
 */

namespace common\helpers;

use Yii;

class AppHelper
{
  public static function formatPrice($price)
  {
    return \yii::$app->formatter->asDecimal(intval($price), 0);
  }

  public static function formatDate($date)
  {
    return Yii::t('app', Yii::$app->formatter->asDatetime($date));
  }

  public static function distanceDate($time)
  {
    if(intval($time) > 0)
      return Yii::$app->formatter->format($time, 'relativeTime');
    else
      return "";

  }

}