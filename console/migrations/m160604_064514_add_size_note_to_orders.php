<?php

use yii\db\Migration;

/**
 * Handles adding size_note to table `orders`.
 */
class m160604_064514_add_size_note_to_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders','size',$this->string());
        $this->addColumn('orders','note',$this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->dropColumn('orders','size');
      $this->dropColumn('orders','note');
    }
}
