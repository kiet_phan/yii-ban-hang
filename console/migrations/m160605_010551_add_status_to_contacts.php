<?php

use yii\db\Migration;

/**
 * Handles adding status to table `contacts`.
 */
class m160605_010551_add_status_to_contacts extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->addColumn('contacts','status',$this->integer()->defaultValue(0));
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropColumn('contacts','status');
  }
}
