<?php

use yii\db\Migration;

class m160317_163036_create_categories_table extends Migration
{
    public function up()
    {
        $this->createTable('categories', [
          'id' => $this->primaryKey(),
          'name' => $this->string()->notNull(),
          'parent_id' => $this->integer()->notNull()->defaultValue(0),
          'level' => $this->integer(),
          'depth' => $this->integer()->defaultValue(0),
          'root' => $this->integer()->defaultValue(0),
          'tree' => $this->integer()->defaultValue(0),
          'lft' => $this->integer()->defaultValue(0),
          'rgt' => $this->integer()->defaultValue(0),
          'created_at' => $this->integer()->notNull(),
          'updated_at' => $this->integer()->notNull(),

        ]);
    }

    public function down()
    {
        $this->dropTable('categories');
    }
}
