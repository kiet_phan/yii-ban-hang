<?php

use yii\db\Migration;

class m160424_041115_create_customers extends Migration
{
  public function up()
  {
    $this->createTable('customers', [
      'id' => $this->primaryKey(),
      'name'=> $this->string(255)->notNull(),
      'email'=> $this->string(255)->unique()->notNull(),
      'phone'=> $this->string(20),
      'address'=>$this->string(255),
      'password'=>$this->string(255)->notNull(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull()

    ]);
  }

  public function down()
  {
    $this->dropTable('customers');
  }
}
