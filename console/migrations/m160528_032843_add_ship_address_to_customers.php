<?php

use yii\db\Migration;

/**
 * Handles adding ship_address to table `customers`.
 */
class m160528_032843_add_ship_address_to_customers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers','ship_address',$this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->dropColumn('customers','ship_address');
    }
}
