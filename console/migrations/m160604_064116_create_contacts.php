<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contacts`.
 */
class m160604_064116_create_contacts extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('contacts', [
      'id' => $this->primaryKey(),
      'name'=>$this->string(),
      'email'=>$this->string(),
      'phone'=>$this->string(),
      'comment'=>$this->text(),
      'address'=>$this->string(),
      'created_at'=>$this->integer(),
      'updated_at'=>$this->integer(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('contacts');
  }
}
