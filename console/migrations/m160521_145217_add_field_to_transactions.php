<?php

use yii\db\Migration;

/**
 * Handles adding field to table `transactions`.
 */
class m160521_145217_add_field_to_transactions extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->addColumn('transactions', 'province',$this->string());
    $this->addColumn('transactions', 'district', $this->string());
    $this->addColumn('transactions', 'first_name', $this->string());
    $this->addColumn('transactions', 'last_name', $this->string());
    $this->addColumn('customers', 'province',$this->string());
    $this->addColumn('customers', 'district', $this->string());
    $this->addColumn('customers', 'first_name', $this->string());
    $this->addColumn('customers', 'last_name', $this->string());
    $this->addColumn('orders', 'unit_price', $this->decimal(15,6)->defaultValue(0.0));
    $this->addColumn('orders', 'discount', $this->decimal(15,6)->defaultValue(0.0));
    $this->dropColumn('transactions','user_name');

    $this->addForeignKey('fk-order-transaction_id', 'orders', 'transaction_id', 'transactions', 'id', 'CASCADE');

  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->addColumn('transactions', 'user_name',$this->string());
    $this->dropColumn('transactions','province');
    $this->dropColumn('transactions','district');
    $this->dropColumn('transactions','first_name');
    $this->dropColumn('transactions','last_name');
    $this->dropColumn('customers','province');
    $this->dropColumn('customers','district');
    $this->dropColumn('customers','first_name');
    $this->dropColumn('customers','last_name');
    $this->dropColumn('orders', 'unit_price');
    $this->dropColumn('orders', 'discount');

    $this->dropForeignKey('fk-order-transaction_id','orders');
  }
}
