<?php

use yii\db\Migration;

/**
 * Handles adding slug_column to table `categories_table`.
 */
class m160716_161827_add_slug_column_to_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('categories', 'slug', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('categories', 'slug');
    }
}
