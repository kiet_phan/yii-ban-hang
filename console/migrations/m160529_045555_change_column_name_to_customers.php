<?php

use yii\db\Migration;

class m160529_045555_change_column_name_to_customers extends Migration
{
  public function up()
  {
    $this->dropColumn('customers','name');

  }

  public function down()
  {
    $this->addColumn('customers','name')->string();
  }

  /*
  // Use safeUp/safeDown to run migration code within a transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
