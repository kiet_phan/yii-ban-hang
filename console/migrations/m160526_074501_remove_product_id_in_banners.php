<?php

use yii\db\Migration;

class m160526_074501_remove_product_id_in_banners extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-banners-product_id','banners');
        $this->dropColumn('banners','product_id');
    }

    public function down()
    {
        $this->addColumn('banners','product_id', $this->integer());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
