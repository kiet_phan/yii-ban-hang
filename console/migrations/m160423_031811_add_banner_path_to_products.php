<?php

use yii\db\Migration;

class m160423_031811_add_banner_path_to_products extends Migration
{
    public function up()
    {
        $this->addColumn('products', 'banner_path', $this->string());
    }

    public function down()
    {
        $this->dropColumn('products', 'banner_path');
    }
}
