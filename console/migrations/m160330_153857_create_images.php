<?php

use yii\db\Migration;

class m160330_153857_create_images extends Migration
{
    public function up()
    {
      $this->createTable('images', [
        'id' => $this->primaryKey(),
        'file_name' => $this->string(),
        'file_path' =>$this->string(),
        'type'=>$this->string(),
        'size'=>$this->integer(),
        'product_id'=> $this->integer(),
        'created_at' => $this->integer()->notNull(),
        'updated_at' => $this->integer()->notNull()

      ]);
      $this->addForeignKey('fk-images-product_id', 'images', 'product_id', 'products', 'id', 'CASCADE');

    }

    public function down()
    {
      $this->dropTable('images');
    }
}
