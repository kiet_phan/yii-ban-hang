<?php

use yii\db\Migration;

class m160327_093724_create_products extends Migration
{
    public function up()
    {
      $this->createTable('products', [
        'id' => $this->primaryKey(),
        'category_id' => $this->integer(),
        'name'=>$this->string(250),
        'slug'=>$this->string(250),
        'price' => $this->decimal(15,4),
        'content' => $this->text(),
        'view'=>$this->integer(),
        'discount' =>$this->decimal(15,4),
        'type_discount' =>$this->integer(),
        'created_at' => $this->integer()->notNull(),
        'updated_at' => $this->integer()->notNull()

      ]);
      $this->addForeignKey('fk-products-product_id', 'products', 'category_id', 'categories', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('products');
    }
}
