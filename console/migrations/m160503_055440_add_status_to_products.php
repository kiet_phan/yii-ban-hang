<?php

use yii\db\Migration;
use yii\db\Schema;


class m160503_055440_add_status_to_products extends Migration
{
  public function up()
  {
    $this->addColumn('products', 'status', Schema::TYPE_SMALLINT . ' DEFAULT 1');
  }

  public function down()
  {
    $this->dropColumn('products', 'status');
  }
}
