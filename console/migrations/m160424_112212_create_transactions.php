<?php

use yii\db\Migration;

class m160424_112212_create_transactions extends Migration
{
  public function up()
  {
    $this->createTable('transactions', [
      'id' => $this->primaryKey(),
      'customer_id'=> $this->integer(),
      'user_name'=>$this->string()->notNull(),
      'user_email'=>$this->string()->notNull(),
      'user_phone'=>$this->string()->notNull(),
      'address'=>$this->string(255)->notNull(),
      'amount'=>$this->decimal(15,4),
      'payment'=>$this->string(40),
      'payment_info'=>$this->text(),
      'status'=>$this->smallInteger()->defaultValue(0)->notNull(),
      'security'=>$this->string(40),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull()
    ]);
    $this->addForeignKey('fk-transactions-transaction_id', 'transactions', 'customer_id', 'customers', 'id', 'CASCADE');
  }

  public function down()
  {
    $this->dropTable('transactions');
  }
}
