<?php

use yii\db\Migration;
use yii\db\mysql\Schema;
class m160508_140729_add_purchased_quantity_in_products extends Migration
{
  public function up()
  {
    $this->addColumn('products', 'purchased_quantity', Schema::TYPE_INTEGER . ' DEFAULT 0');
    $this->addColumn('products', 'summary', $this->text());
    $this->alterColumn('products','discount',$this->decimal(15,6)->defaultValue(0.0));
  }

  public function down()
  {
    $this->dropColumn('products','purchased_quantity');
    $this->dropColumn('products','summary');
    $this->alterColumn('products','discount',$this->decimal(15,6)->defaultValue(0.0));
  }

  /*
  // Use safeUp/safeDown to run migration code within a transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
