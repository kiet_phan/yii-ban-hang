<?php

use yii\db\Migration;

class m160317_174319_create_employees extends Migration
{
    public function up()
    {
        $this->createTable('employees', [
          'id' => $this->primaryKey(),
          'name' => $this->string()->notNull(),
          'auth_key' => $this->string(255),
          'password' => $this->string()->notNull(),
          'password_reset_token' => $this->string()->unique(),
          'email' => $this->string()->notNull()->unique(),
          'role'=> $this->smallInteger()->notNull()->defaultValue(0),
          'status' => $this->smallInteger()->notNull()->defaultValue(10),
          'created_at' => $this->integer()->notNull(),
          'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('employees');
    }
}
