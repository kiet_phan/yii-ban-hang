<?php

use yii\db\Migration;

class m160424_113922_create_orders extends Migration
{
  public function up()
  {
    $this->createTable('orders', [
      'id' => $this->primaryKey(),
      'transaction_id' => $this->integer(),
      "product_id" => $this->integer()->notNull(),
      "qty" => $this->integer(),
      "amount" => $this->decimal(15, 4),
      "data" => $this->text(),
      'status' => $this->integer()->defaultValue(0)
    ]);
    $this->addForeignKey('fk-orders-transaction_id', 'orders', 'transaction_id', 'transactions', 'id', 'CASCADE');
    $this->addForeignKey('fk-orders-product_id', 'orders', 'product_id', 'products', 'id', 'CASCADE');

  }

  public function down()
  {
    $this->dropTable('orders');
  }
}
