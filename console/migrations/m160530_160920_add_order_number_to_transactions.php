<?php

use yii\db\Migration;

/**
 * Handles adding order_number to table `transactions`.
 */
class m160530_160920_add_order_number_to_transactions extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->addColumn('transactions','order_num',$this->string()->unique());
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropColumn('transactions','order_num');

  }
}
