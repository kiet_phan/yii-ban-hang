<?php

use yii\db\Migration;

/**
 * Handles the creation for table `price_lits`.
 */
class m160525_165233_create_price_lits extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('price_list', [
      'id' => $this->primaryKey(),
      'file_name' => $this->string(),
      'file_path' => $this->string(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull()
    ]);
    $this->addColumn('banners', 'file_name',$this->string());
    $this->addColumn('banners', 'file_path', $this->string());
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('price_list');
    $this->dropColumn('banners', 'file_name');
    $this->dropColumn('banners', 'file_path');
  }
}
