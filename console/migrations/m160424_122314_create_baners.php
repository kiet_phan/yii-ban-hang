<?php

use yii\db\Migration;

class m160424_122314_create_baners extends Migration
{
  public function up()
  {
    $this->createTable('banners', [
      'id' => $this->primaryKey(),
      'product_id'=>$this->integer()->notNull(),
      'priority' => $this->integer()->defaultValue(0),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull()

    ]);
    $this->addForeignKey(
      'fk-banners-product_id',
      'banners',
      'product_id',
      'products',
      'id',
      'CASCADE'
    );
  }

  public function down()
  {
    $this->dropTable('banners');
  }
}
