<?php
/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 4/14/16
 * Time: 10:40 PM
 */

namespace console\controllers;



use common\models\Transactions;
use yii\console\Controller;
use common\models\Categories;
use yii\helpers\FileHelper;
use Yii;

class CategoriesController extends Controller
{
  public function actionIndex()
  {
//    $root = Categories::findOne(3);
//    $russia = new Categories(['name' => 'Áo Sơ my ','parent_id'=>3]);
//    $russia->prependTo($root);
  }
  public function actionText()
  {
    $class_name = "ProductController";
    $name = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class_name));
    $basePath = \Yii::getAlias("@backend");
    $imagePath = $basePath.DS."web".DS."img".DS."uploads/images";
    echo FileHelper::createDirectory($imagePath,0777), "=====";
  }
  public function actionAddSlug()
  {
    $categories = Categories::find()->all();
    foreach ($categories as $obj) {
      $cate = Categories::findOne($obj->id);
      $cate->save();

    }
  }

  public function actionUpdateOrderNumber(){
    $orders = Transactions::find()->all();
    foreach (Transactions::find()->each(10) as $order) {

      if(empty($order->order_num)){

        Yii::$app->db->createCommand()->update('transactions', ['order_num' => $order->getUniqueNumber()], ["id"=>$order->id])->execute();

      }
      // $customer is a Customer object
    }
  }
}