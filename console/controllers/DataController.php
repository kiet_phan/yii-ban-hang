<?php
/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 4/14/16
 * Time: 10:40 PM
 */

namespace console\controllers;



use common\models\Employees;
use yii\console\Controller;
use common\models\Categories;
use yii\helpers\FileHelper;

class DataController extends Controller
{
  public function actionIndex()
  {
    $user = new Employees(['name' => 'Hai Huynh','email'=>'admin@example.com','role'=>'0','status'=>1,'password'=>'123123']);
    $user->save();
  }
  public function actionText()
  {
    $class_name = "ProductController";
    $name = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class_name));
    $basePath = \Yii::getAlias("@backend");
    $imagePath = $basePath.DS."web".DS."img".DS."uploads/images";
    echo FileHelper::createDirectory($imagePath,0777), "=====";
  }
}