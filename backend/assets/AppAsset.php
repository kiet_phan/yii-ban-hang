<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/font-awesome/css/font-awesome.min.css',
    'plugins/simple-line-icons/simple-line-icons.min.css',
    'plugins/bootstrap/css/bootstrap.min.css',
    'plugins/uniform/css/uniform.default.css',
    'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
    'plugins/fullcalendar/fullcalendar.min.css',
    'plugins/jqvmap/jqvmap/jqvmap.css',
    'plugins/select2/dist/css/select2.min.css',
    'pages/css/tasks.css',
    'css/themes/default.css',
    'css/components.css',
    'css/layout.css',
    'css/custom.css',
    'css/plugins.css',

  ];
  public $js = [
    "plugins/respond.min.js",
    "plugins/excanvas.min.js",
//    "plugins/jquery.min.js",
    "plugins/jquery-migrate.min.js",
    "plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js",
    "plugins/bootstrap/js/bootstrap.min.js",
    "plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
    "plugins/jquery-slimscroll/jquery.slimscroll.min.js",
    "plugins/jquery.blockui.min.js",
    "plugins/jquery.cokie.min.js",
    "plugins/uniform/jquery.uniform.min.js",
    "plugins/bootstrap-switch/js/bootstrap-switch.min.js",
//    "plugins/jqvmap/jqvmap/jquery.vmap.js",
//    "plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js",
//    "plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js",
//    "plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js",
//    "plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js",
//    "plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js",
    "plugins/select2/dist/js/select2.full.min.js",
    "plugins/flot/jquery.flot.min.js",
    "plugins/flot/jquery.flot.resize.min.js",
    "plugins/flot/jquery.flot.categories.min.js",
    "plugins/jquery.pulsate.min.js",
    "plugins/bootstrap-daterangepicker/moment.min.js",
    "plugins/bootstrap-daterangepicker/moment.min.js",
    "plugins/bootstrap-daterangepicker/daterangepicker.js",
    "plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
    "plugins/datatables/media/js/jquery.dataTables.min.js",
    "plugins/fullcalendar/fullcalendar.min.js",
    "plugins/jquery-easypiechart/jquery.easypiechart.min.js",
    "plugins/jquery-easypiechart/jquery.easypiechart.min.js",
    "plugins/jquery.sparkline.min.js",
    "js/metronic.js",
    "js/layout.js",
    "js/quick-sidebar.js",
    "js/demo.js",
    "js/themes/index.js",
    "js/themes/tasks.js"
  ];
  public $depends = [
    'yii\web\JqueryAsset',
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
}