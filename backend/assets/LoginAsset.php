<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/font-awesome/css/font-awesome.min.css',
    'plugins/simple-line-icons/simple-line-icons.min.css',
    'plugins/bootstrap/css/bootstrap.min.css',
    'plugins/uniform/css/uniform.default.css',
    'css/components.css',
    'css/layout.css',
    'css/themes/default.css',
    'css/custom.css',
    'pages/css/login.css',
  ];
  public $js = [
    "plugins/respond.min.js",
    "plugins/excanvas.min.js",
//    "plugins/jquery.min.js",
    "plugins/jquery-migrate.min.js",
    "plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js",
    "plugins/bootstrap/js/bootstrap.min.js",
    "plugins/jquery.blockui.min.js",
    'plugins/uniform/jquery.uniform.min.js',
    'plugins/jquery-validation/js/jquery.validate.min.js',
    'js/metronic.js',
    'js/layout.js',
    'pages/scripts/login.js'


  ];
  public $depends = [
    'yii\web\JqueryAsset',
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
}