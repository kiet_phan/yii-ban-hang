/**
 * Created by phan-kiet on 5/2/16.
 */
$('body').ready(function() {
  $('#select2Product').select2({
    placeholder: "Select a Product",
    ajax: {
      url: baseUrl+ "/products/get-product",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          search: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        console.log(data)
        params.page = params.page || 1;

        return {
          results: data.items,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
  });
  $('ul.status-menu .item-menu').click(function(evt){
    evt.preventDefault();
    var text = $(this).text();
    $parrentMenu = $(this).closest('.drop-down-status');
    $parrentMenu.find("[name='order_status']").val($(this).attr('data-value'));
    $parrentMenu.find('button.dropdown-toggle').html(text+ '<i class="fa fa-angle-down"></i>');
  });
  $("#updateStatusOrder").on('click',function(evt){
    var url = $(this).attr('data-source')
    var status = $('[name="order_status"]').val()
    var param = "status=" + status
    window.location.href = addParameterToURL(url,param);
  })
});
function addParameterToURL(url,param){
  _url = url;
  _url += (_url.split('?')[1] ? '&':'?') + param;
  return _url;
}