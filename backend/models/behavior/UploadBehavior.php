<?php
namespace backend\models\behavior;
use \yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 4/18/16
 * Time: 5:51 PM
 */
class UploadBehavior extends Behavior
{
  public $thumbs = [
    'thumb' => ['width' => 700, 'height' => 850, 'quality' => 90],
    'preview' => ['width' => 200, 'height' => 250],
  ];
  public $field = "file_path";
  public $quality = 100;
  public function events()
  {
    return [
      ActiveRecord::EVENT_AFTER_INSERT =>'afterSave',
      ActiveRecord::EVENT_AFTER_UPDATE =>'afterSave',
      ActiveRecord::EVENT_BEFORE_INSERT =>"beforeSave",
      ActiveRecord::EVENT_AFTER_UPDATE => "beforeSave",
      ActiveRecord::EVENT_BEFORE_DELETE => "beforeDelete"
    ];
  }
  public function beforeSave($event){

  }
  public function beforeDelete($event){
    $modelObject = $this->owner;
    $className = $modelObject->formName();
    $foderName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    $basePath = \Yii::getAlias('@frontend').DS.'web';
    $imagePath = $basePath.DS."uploads".DS."img".DS."uploads".DS.$foderName.DS.$modelObject->id;
    FileHelper::removeDirectory($imagePath);
  }
  public function afterSave($event){
    $connection = Yii::$app->db;
    $modelObject = $this->owner;
    $field = $this->field;
    $extention = pathinfo($modelObject->$field->name, PATHINFO_EXTENSION);


    $dirPath =  $this->generateForder();
    $imageName = $modelObject->id."_".md5(time()).".".$extention;
    $imagePath = $dirPath.DS.$imageName;
    $modelObject->$field->saveAs($imagePath);
    $this->createThumbs($imagePath);
    $connection->createCommand()->update('images', ['file_path' => $imageName], ["id"=>$modelObject->id])->execute();

//    echo "<pre>";
//    var_dump($modelObject->$field);die;
//    $quality = $this->getCorrectQuality(pathinfo($modelObject->$field->name, PATHINFO_EXTENSION));
  }
  protected function createThumbs($imagePath)
  {
    $path = dirname($imagePath);
    $image_name = basename($imagePath);
    foreach ($this->thumbs as $profile => $config) {

      $thumbPath = $path.DS.$profile.'_'.$image_name;
      $this->generateImageThumb($config, $imagePath, $thumbPath);
    }
  }
  public function generateForder(){

    $modelObject = $this->owner;
    $className = $modelObject->formName();
    $foderName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    $basePath = \Yii::getAlias('@frontend').DS.'web';
    $imagePath = $basePath.DS."uploads".DS."img".DS."uploads".DS.$foderName.DS.$modelObject->id;
    FileHelper::createDirectory($imagePath,0777);
    return $imagePath;
//    $imageDir = $basePath +

  }

  protected function generateImageThumb($config, $path, $thumbPath)
  {
    $width = ArrayHelper::getValue($config, 'width');
    $height = ArrayHelper::getValue($config, 'height');
    $quality = ArrayHelper::getValue($config, 'quality', 100);

    if (!$width || !$height) {
      $image = Image::getImagine()->open($path);
      $ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
      if ($width) {
        $height = ceil($width / $ratio);
      } else {
        $width = ceil($height * $ratio);
      }
    }

    // Fix error "PHP GD Allowed memory size exhausted".
    ini_set('memory_limit', '512M');
    Image::thumbnail($path, $width, $height)->save($thumbPath, ['quality' => $quality]);
  }
  protected function getThumbFileName($filename, $profile = 'thumb')
  {
    return $profile . '-' . $filename;
  }
  private function getCorrectQuality($extension, $quality = 100) {
    $quality_arr = ['jpeg_quality' => $quality];
    if (strtolower($extension) == 'png') {
      $quality = 9 - round(($quality / 100) * 9);
      $quality_arr = ['png_compression_level' => $quality];
    }

    return $quality_arr;
  }

}