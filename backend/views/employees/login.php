<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\widgets\ActiveForm;

\backend\assets\LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
  <!DOCTYPE html>
  <!--[if IE 8]>
  <html lang="en" class="ie8 no-js"> <![endif]-->
  <!--[if IE 9]>
  <html lang="en" class="ie9 no-js"> <![endif]-->
  <!--[if !IE]><!-->
  <html lang="en" class="no-js">
  <!--<![endif]-->
  <!-- BEGIN HEAD -->
  <head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <body class="login">
  <?php $this->beginBody() ?>
  <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
  <div class="menu-toggler sidebar-toggler">
  </div>
  <!-- END SIDEBAR TOGGLER BUTTON -->
  <!-- BEGIN LOGO -->
  <div class="logo">
    <a href="index.html">
      <?= Html::img('@web/images/logo-big.png', ['alt' => 'logo']) ?>
    </a>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' =>['class' => 'login-form'] ]); ?>
    <h3 class="form-title">Sign In</h3>
    <div class="alert alert-danger display-hide">
      <button class="close" data-close="alert"></button>
  			<span>
  			Enter any username and password. </span>
    </div>
    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div class="form-actions">
      <?= Html::submitButton('Login', ['class' => 'btn btn-success uppercase', 'name' => 'login-button']) ?>
      <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
    </div>

    <?php ActiveForm::end(); ?>
    <form class="forget-form" action="index.html" method="post">
      <h3>Forget Password ?</h3>
      <p>
        Enter your e-mail address below to reset your password.
      </p>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
      </div>
      <div class="form-actions">
        <button type="button" id="back-btn" class="btn btn-default">Back</button>
        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
      </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
  </div>
  <div class="copyright">
    2014 © Metronic. Admin Dashboard Template.
  </div>
  <?=
  $this->registerJs(
    'jQuery(document).ready(function() {
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Login.init();
   });'
  );

  ?>
  <?php $this->endBody() ?>
  </body>
  </html>
<?php $this->endPage() ?>