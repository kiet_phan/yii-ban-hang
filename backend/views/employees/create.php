<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Employees */

$this->title = Yii::t('app', 'Create Employees');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
use common\widgets\Alert;
?>
<?= Alert::widget() ?>
<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body">
          <?= $this->render('_form', [
            'model' => $model,
          ]) ?>

        </div>
      </section>

    </div>
  </div>



</section>
