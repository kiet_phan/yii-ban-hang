<?php

use yii\helpers\Html;
use yii\grid\GridView;
use faryshta\data\EnumColumn;
use common\models\Employees;
use backend\helper\CommonHelper;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a(Yii::t('app', 'Create Employees'), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions'=>['class'=>'table table-striped table-advance table-hover'],
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'id',
      'name',
      'email:email',
      [
        'attribute' => 'role',
        'value' =>  function ($data) {
          return Employees::enums()['role'][$data->role];
      }],
      [
        'attribute' => 'status',
        'format' => 'raw',
        'value' =>  function ($data) {
          return CommonHelper::generatStatus($data->status);
        }],
      'created_at',
      'updated_at',

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>
  </section>
