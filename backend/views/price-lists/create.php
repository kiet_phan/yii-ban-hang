<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PriceList */

$this->title = 'Upload bảng giá';
$this->params['breadcrumbs'][] = ['label' => 'Bảng giá', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
