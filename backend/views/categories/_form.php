<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use faryshta\widgets\EnumDropdown;
use faryshta\widgets\EnumRadio;
/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>



<?php
$form = ActiveForm::begin([
  'id' => 'login-form-horizontal',
  'type' => ActiveForm::TYPE_HORIZONTAL,
  'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

]);
?>


<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'parent_id')->dropDownList(\backend\helper\CommonHelper::categoriesOption($categories)) ?>


<div class="form-group">
  <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


