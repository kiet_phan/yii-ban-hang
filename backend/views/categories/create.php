<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = Yii::t('app', 'Create Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body">
          <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories
          ]) ?>

        </div>
      </section>

    </div>
  </div>


</section>