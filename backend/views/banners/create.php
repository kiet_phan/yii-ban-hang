<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Banners */

$this->title = Yii::t('app', 'Create Banners');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <?= Html::encode($this->title) ?>
                </header>
                <div class="panel-body">
                    <?= $this->render('_form', [
                      'model' => $model,
                    ]) ?>

                </div>
            </section>

        </div>
    </div>


</section>
