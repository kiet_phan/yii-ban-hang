<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
use kartik\widgets\FileInput;

?>


<?php $form = ActiveForm::begin([
  'id' => 'login-form-horizontal',
  'type' => ActiveForm::TYPE_HORIZONTAL,
  'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
  'options' => ['enctype' => 'multipart/form-data']
]); ?>


<?= $form->field($model, 'priority')->textInput() ?>
<?= $form->field($model, 'banner_file')->fileInput() ?>


<div class="form-group">
  <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
<?=$this->registerJsFile("@web/js/client.js",['depends' => [\yii\web\JqueryAsset::className()]]) ?>
