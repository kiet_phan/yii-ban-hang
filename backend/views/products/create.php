<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Create Products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body">
          <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories,
            'model_images'=>$model_images
          ]) ?>

        </div>
      </section>

    </div>
  </div>


</section>
