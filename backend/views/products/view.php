<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\helper\CommonHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      'category_id',
      'name',
      'price',
      'content:ntext',
      'view',
      'discount',
      'type_discount',
      'created_at',
      'updated_at',
    ],
  ]) ?>

</div>
<div class="tab-pane" id="tab_images">
  <div class="alert alert-success margin-bottom-10">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <i class="fa fa-warning fa-lg"></i> Image type and information need to be specified.
  </div>
  <div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">
    <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
      <i class="fa fa-plus"></i> Select Files </a>
    <a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green">
      <i class="fa fa-share"></i> Upload Files </a>
  </div>
  <div class="row">
    <div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
    </div>
  </div>
  <table class="table table-bordered table-hover">
    <thead>
    <tr role="row" class="heading">
      <th width="8%">
        Image
      </th>
      <th width="25%">
        Label
      </th>
      <th width="8%">
        Sort Order
      </th>
      <th width="10%">
        Base Image
      </th>
      <th width="10%">
        Small Image
      </th>
      <th width="10%">
        Thumbnail
      </th>
      <th width="10%">
      </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($images as $image): ?>
    <tr>
      <td>
        <a href="../../assets/admin/pages/media/works/img1.jpg" class="fancybox-button" data-rel="fancybox-button">
          <?= Html::img('@image_url'.CommonHelper::imageUrl($image,$image->file_path), [
            'alt' => $model->name,
            'class'=>'img-responsive'
          ]) ?>
        </a>
      </td>
      <td>
        <input type="text" class="form-control" name="product[images][1][label]" value="<?=$image->file_name?>">
      </td>
      <td>
        <input type="text" class="form-control" name="product[images][1][sort_order]" value="1">
      </td>
      <td>
        <label>
          <input type="radio" name="product[images][1][image_type]" value="1">
        </label>
      </td>
      <td>
        <label>
          <input type="radio" name="product[images][1][image_type]" value="2">
        </label>
      </td>
      <td>
        <label>
          <input type="radio" name="product[images][1][image_type]" value="3" checked>
        </label>
      </td>
      <td>
        <a href="<?=Url::toRoute(['images/delete','id'=>$image->id])?>" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"> <i class="fa fa-times"></i> Remove </a>

      </td>
    </tr>
    <?php endforeach; ?>

    </tbody>
  </table>
</div>