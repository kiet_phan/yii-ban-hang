<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
use kartik\widgets\FileInput;
?>


<?php $form = ActiveForm::begin([
  'id' => 'login-form-horizontal',
  'type' => ActiveForm::TYPE_HORIZONTAL,
  'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
  'options' => ['enctype' => 'multipart/form-data']
]); ?>

<?= $form->field($model, 'category_id')->dropDownList(\backend\helper\CommonHelper::categoriesOption($categories)) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'summary')->textarea(['rows' => 6]) ?>
<?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'type_discount')->textInput() ?>
<?= $form->field($model, 'image_list[]')->widget(FileInput::classname(), [
  'options' => [
    'multiple' => true,
    'accept' => 'image/*',
  ],
  'pluginOptions' => [
    'previewFileType' => 'image',
    'showUpload' => false,
    'maxFileCount' => 10
  ],
  'resizeImages'=>true,



]); ?>




<div class="form-group">
  <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


