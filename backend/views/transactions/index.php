<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactions-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      'customer_id',
      'user_email:email',
      'user_phone',
      'address',
      [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model) {
          return \common\models\Transactions::enums()['status'][$model->status];
        },
      ],

      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>
</div>
