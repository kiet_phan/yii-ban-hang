<div class="actions">
  <a href="#" class="btn default yellow-stripe">
    <i class="fa fa-angle-left"></i>
								<span class="hidden-480">
								Back </span>
  </a>
  <div class="btn-group">
    <a class="btn default yellow-stripe" href="#" data-toggle="dropdown">
      <i class="fa fa-cog"></i>
									<span class="hidden-480">
									Tools </span>
      <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-right">
      <li>
        <a href="#">
          Export to Excel </a>
      </li>
      <li>
        <a href="#">
          Export to CSV </a>
      </li>
      <li>
        <a href="#">
          Export to XML </a>
      </li>
      <li class="divider">
      </li>
      <li>
        <a href="#">
          Print Invoice </a>
      </li>
    </ul>
  </div>
</div>