<?php
use yii\helpers\Url;
use common\helpers\AppHelper;

?>


<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="portlet grey-cascade box">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-cogs"></i>Shopping Cart
        </div>
        <div class="actions">
          <a href="#" class="btn btn-default btn-sm">
            <i class="fa fa-pencil"></i> Edit </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered table-striped">
            <thead>
            <tr>
              <th>Product</th>
              <th>Item Status</th>
              <th>Unit Price</th>
              <th>Quantity</th>
              <th> Size</th>
              <th> Note</th>
              <th>Sub total</th>
              <th> Discount Amount per product</th>

              <th> Total </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $t_subTotal = 0;
            $t_toTal = 0;
            $discount = 0;
            foreach($orders as $order):
              $product = $order->product;
              $sub_total = $order->unit_price * $order->qty;
              $total = $order->amount * $order->qty;
              $t_toTal += $total;
              $t_subTotal +=$sub_total;
              $discount += ($order->discount * $order->qty);
              ?>
              <tr>
                <td><a href="<?= Url::to(['products/view','id'=>$product->id]) ?>"> <?=$product->name?></a> </td>
                <td><span class="label label-sm label-success">	Available</span></td>
                <td> <?=AppHelper::formatPrice($order->unit_price)?></td>
                <td><?=$order->qty?></td>
                <td><?=$order->size?></td>
                <td><?=$order->note?></td>
                <!--            <td>2.00$</td>-->
                <td> <?=AppHelper::formatPrice($sub_total)?></td>
                <td><?=AppHelper::formatPrice($order->discount)?></td>
                <td><?=AppHelper::formatPrice($total)?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
  </div>
  <div class="col-md-6">
    <div class="well">
      <div class="row static-info align-reverse">
        <div class="col-md-8 name">Sub Total:</div>
        <div class="col-md-3 value"> <?=AppHelper::formatPrice($t_subTotal)?></div>

      </div>
      <div class="row static-info align-reverse">
        <div class="col-md-8 name">Discount:</div>
        <div class="col-md-3 value"><?=AppHelper::formatPrice($discount)?></div>
      </div>
<!--      <div class="row static-info align-reverse">-->
<!--        <div class="col-md-8 name">-->
<!--          Grand Total:-->
<!--        </div>-->
<!--        <div class="col-md-3 value">-->
<!--          $1,260.00-->
<!--        </div>-->
<!--      </div>-->
      <div class="row static-info align-reverse">
        <div class="col-md-8 name">Total Paid:</div>
        <div class="col-md-3 value"> <?=AppHelper::formatPrice($t_toTal)?></div>
      </div>
<!--      <div class="row static-info align-reverse">-->
<!--        <div class="col-md-8 name">-->
<!--          Total Refunded:-->
<!--        </div>-->
<!--        <div class="col-md-3 value">-->
<!--          $0.00-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="row static-info align-reverse">-->
<!--        <div class="col-md-8 name">-->
<!--          Total Due:-->
<!--        </div>-->
<!--        <div class="col-md-3 value">-->
<!--          $1,124.50-->
<!--        </div>-->
<!--      </div>-->
    </div>
  </div>
</div>