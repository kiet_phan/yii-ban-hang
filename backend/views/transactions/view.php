<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;
use common\helpers\AppHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Transactions */
use yii\helpers\Url;
$this->title = $transaction->order_num;
$this->params['breadcrumbs'][] = ['label' => 'Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$list_status = $transaction->enums()['status'];
?>
<?=$this->render('_header') ?>

<h3 class="page-title">
  Order View <small>view order details</small>
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="index.html">Home</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">eCommerce</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Order View</a>
    </li>
  </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
  <div class="col-md-12">
    <div class="btn-group drop-down-status">
      <button class="btn green dropdown-toggle" type="button" data-toggle="dropdown"><?= $list_status[$transaction->status] ?> <i class="fa fa-angle-down"></i>
      </button>
      <input type="hidden" name="order_status" value="<?=$transaction->status ?>">
      <ul class="dropdown-menu status-menu" role="menu">
        <?php foreach($list_status as $key=>$status): ?>
        <li>
          <a class="item-menu" href="#" data-value = "<?= $key?>"><?=$status?> </a>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <button type="button" data-source="<?=Url::to(['transactions/change-status','id'=>$transaction->id])?>" class="btn btn-primary" id="updateStatusOrder">Change status</button>
  </div>
  <div class="col-md-12">
    <!-- Begin: life time stats -->
    <div class="portlet">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-shopping-cart"></i>
          Order #<?=$transaction->order_num?>
          <span class="hidden-480"><?=AppHelper::formatDate($transaction->created_at)?></span>
        </div>
<!--        --><?php //$this->render('_action_order', []) ?>
      </div>
      <div class="portlet-body">
        <div class="tabbable">
          <ul class="nav nav-tabs nav-tabs-lg">
            <li class="active">
              <a href="#tab_1" data-toggle="tab">
                Details </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <div class="portlet yellow-crusta box">
                    <div class="portlet-title">
                      <div class="caption">
                        <i class="fa fa-cogs"></i>Order Details
                      </div>
                      <div class="actions">
                        <a href="#" class="btn btn-default btn-sm">
                          <i class="fa fa-pencil"></i> Edit </a>
                      </div>
                    </div>
                    <div class="portlet-body">
                      <div class="row static-info">
                        <div class="col-md-5 name">Order #:</div>
                        <div class="col-md-7 value">
                          <?=$transaction->order_num?>
                          <span class="label label-info label-sm">Email confirmation was sent </span>
                        </div>
                      </div>
                      <div class="row static-info">
                        <div class="col-md-5 name">
                          Order Date & Time:
                        </div>
                        <div class="col-md-7 value"><?=AppHelper::formatDate($transaction->created_at)?></div>
                      </div>
                      <div class="row static-info">
                        <div class="col-md-5 name">
                          Order Status:
                        </div>
                        <div class="col-md-7 value">
																<span class="label label-success">
																Closed </span>
                        </div>
                      </div>
                      <div class="row static-info">
                        <div class="col-md-5 name">
                          Grand Total:
                        </div>
                        <div class="col-md-7 value">
                          <?=AppHelper::formatPrice($transaction->amount)?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="portlet red-sunglo box">
                    <div class="portlet-title">
                      <div class="caption">
                        <i class="fa fa-cogs"></i>Shipping Address
                      </div>
                      <div class="actions">
                        <a href="#" class="btn btn-default btn-sm">
                          <i class="fa fa-pencil"></i> Edit </a>
                      </div>
                    </div>
                    <div class="portlet-body">
                      <div class="row static-info">
                        <div class="col-md-12 value">
                          <?= $transaction->getName()?><br>
                          <?= $transaction->address?><br>
                          <?= $transaction->district?><br>
                          <?= $transaction->province?><br>
                          Việt Nam<br>
                          Điện thoại: <?= $transaction->user_phone?><br>
                          Mail: <?= $transaction->user_email?><br>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">
                <?php if(!empty($customer)): ?>
                  <div class="col-md-6 col-sm-12">
                    <div class="portlet blue-hoki box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>Customer Information
                        </div>
                        <div class="actions">
                          <a href="#" class="btn btn-default btn-sm">
                            <i class="fa fa-pencil"></i> Edit </a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-5 name">
                            Customer Name:
                          </div>
                          <div class="col-md-7 value">
                            Jhon Doe
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                            Email:
                          </div>
                          <div class="col-md-7 value">
                            jhon@doe.com
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                            State:
                          </div>
                          <div class="col-md-7 value">
                            New York
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                            Phone Number:
                          </div>
                          <div class="col-md-7 value">
                            12234389
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
              <?=$this->render('_order_detail',['orders'=>$orders])?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End: life time stats -->
  </div>
</div>
<?=$this->registerJsFile('@web/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', [
  'depends' => [\backend\assets\AppAsset::className()]
]); ?>
<?=$this->registerJsFile('@web/js/datatable.js', ['depends' => [backend\assets\AppAsset::className()]]); ?>
<?=$this->registerJsFile('@web/js/themes/ecommerce-orders-view.js', ['depends' => [backend\assets\AppAsset::className()]]); ?>

<?=
$this->registerJs(
'jQuery(document).ready(function() {
  // EcommerceOrdersView.init();
  });',View::POS_END
);

?>
<?=$this->registerJsFile("@web/js/client.js",['depends' => [\yii\web\JqueryAsset::className()]]) ?>
