<?php

/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 4/10/16
 * Time: 11:21 PM
 */
namespace backend\helper;
use yii\helpers\Html;
use Yii;
class CommonHelper
{
  public static function generatStatus($status = 0){
    $options = ['class' => 'label label-mini'];
    if ($status == 0){
      Html::removeCssClass($options, 'label-danger');
      Html::addCssClass($options, 'label-success');
      $text = "Inactive";
      $html = "<span class=\"label label-success label-mini\">Active</span>";
    }else{
      Html::removeCssClass($options, 'label-success');
      Html::addCssClass($options, 'label-danger');
      $text = "Active";
    }

    return  Html::tag('span', $text, $options);
  }
  public static function categoriesOption($categories){
    $depth=0;
    $arr_option = [];
    foreach($categories as $n=>$category)
    {
      $arr_option[$category->id] = str_repeat('__',$category->depth).$category->name;
    }
    return $arr_option;
  }
  public static function getActiveMenu($list_controller){
    $currentController = \Yii::$app->controller->id;
    $class = '';
    if(in_array($currentController,$list_controller)){
      $class = 'active';
      if(count($list_controller) > 1) $class = "open";
    }
    return $class;
  }
  public static function imageUrl($model, $image_name)
  {
    $className = $model->formName();
    $foderName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    $image_path = '/uploads/img/uploads/' . $foderName . DS . $model->id . DS . $image_name;
    return $image_path;
  }

}