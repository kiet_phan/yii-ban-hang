<?php

namespace backend\controllers;

use common\models\Images;
use Yii;
use common\models\Products;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Categories;
use yii\web\UploadedFile;
use yii\data\Pagination;
/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
//        'actions' => [
//          'delete' => ['POST'],
//        ],POST
      ],
    ];
  }

  /**
   * Lists all Products models.
   * @return mixed
   */
  public function actionIndex()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Products::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Products model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $product = $this->findModel($id);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'images' =>$product->images
    ]);
  }

  /**
   * Creates a new Products model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Products();
    $categories = Categories::find()->orderBy('lft')->all();
    $model_images = new Images();
    $result =  null;
    if ($model->load(Yii::$app->request->post())) {
      $image_list = UploadedFile::getInstances($model, 'image_list');
      $model->banner =  UploadedFile::getInstance($model, 'banner');
      $model->image_list =$image_list;
      $result =  $model->save();

    }
    if($result){
      return $this->redirect(['view', 'id' => $model->id]);
    }else{
      return $this->render('create', [
        'model' => $model,
        'categories' => $categories,
        'model_images'=>$model_images
      ]);
    }
  }

  /**
   * Updates an existing Products model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {

    $model = $this->findModel($id);
    $categories = Categories::find()->orderBy('lft')->all();
    $model_images = new Images();
    $result =  null;
    if ($model->load(Yii::$app->request->post())) {

      $image_list = UploadedFile::getInstances($model, 'image_list');
      $model->banner =  UploadedFile::getInstance($model, 'banner');
      $model->image_list =$image_list;
      $result =  $model->save();

    }
    if($result){
      return $this->redirect(['view', 'id' => $model->id]);
    }else{
      return $this->render('create', [
        'model' => $model,
        'categories' => $categories,
        'model_images'=>$model_images
      ]);
    }
  }

  /**
   * Deletes an existing Products model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }
  public function actionGetProduct(){
    $request = Yii::$app->request;
    $search = $request->get('search');
    $conditions = ['or'];

    $query = Products::find()->select(['id','name']);
    if(!empty($search)){
      $query->andFilterWhere([
        'or',
        ['like', 'LOWER(products.name)', strtolower($search)],
        ['like', 'LOWER(products.slug)', strtolower($search)]
      ]);
    }
    $countQuery = clone $query;
    $pages = new Pagination(['totalCount' => $countQuery->count()]);
    $products = $query->offset($pages->offset)
      ->limit($pages->limit)
      ->all();

    $result = [];
    foreach($products as $item){
      array_push($result,['id'=>$item->id,'text'=>$item->name]);
    }
    $response = Yii::$app->response;
    $response->format = \yii\web\Response::FORMAT_JSON;
    $response->data = ['items'=>$result,'total_count'=>$countQuery->count()];

    return $response;
  }

  /**
   * Finds the Products model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Products the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Products::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
