<?php

namespace backend\controllers;

use Yii;
use common\models\Banners;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * BannersController implements the CRUD actions for Banners model.
 */
class BannersController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all Banners models.
   * @return mixed
   */
  public function actionIndex()
  {
    $banner = Banners::findOne(1);
    $dataProvider = new ActiveDataProvider([
      'query' => Banners::find()
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Banners model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Banners model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Banners();

    if ($model->load(Yii::$app->request->post())) {
      $model->banner_file =  UploadedFile::getInstance($model, 'banner_file');
      $model->save();
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Banners model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $connection = Yii::$app->db;
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      $file =  UploadedFile::getInstance($model, 'banner_file');
      if($file){
        $basePath = \Yii::getAlias('@frontend').DS.'web';
        $imagePath = $basePath.DS."uploads".DS."img".DS."uploads".DS."banners".DS.$model->id;

        FileHelper::removeDirectory($imagePath);
        FileHelper::createDirectory($imagePath,0777);

        $file->saveAs($imagePath.DS. $file->name);
        $connection->createCommand()->update('banners', ['file_path' => $file->name,'priority'=>$model->priority], ["id"=>$model->id])->execute();
      }else{
        $model->save();
      }



      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      // echo "<pre>";
      // var_dump($model);die;
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Banners model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Banners model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Banners the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Banners::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
