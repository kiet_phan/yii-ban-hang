<?php
$params = array_merge(
  require(__DIR__ . '/../../common/config/params.php'),
  require(__DIR__ . '/../../common/config/params-local.php'),
  require(__DIR__ . '/params.php'),
  require(__DIR__ . '/params-local.php')
);

return [
  'id' => 'app-backend',
  'basePath' => dirname(__DIR__),
  'controllerNamespace' => 'backend\controllers',
  'bootstrap' => ['log'],
  'modules' => [],
  'layout' => 'default_layout',
  'components' => [
    'user' => [
      'identityClass' => 'common\models\Employees',
      'enableAutoLogin' => false,
      'loginUrl' => ['employees/login'],
      'identityCookie' => [
        'name' => '_backendUser', // unique for backend
        'path' => dirname(__DIR__) . '/web'  // correct path for the backend app.
      ]
    ],
    'session' => [
      'name' => '_backendSessionId', // unique for backend
      'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
        '<controller>/<action>/<id:\d+>' => '<controller>/<action>'
      ],

    ],
    'urlManagerFrontend' => [
      'class' => 'yii\web\urlManager',
      'baseUrl' => FRONEND_URL,
      'enablePrettyUrl' => true,
      'showScriptName' => false,
    ],
  ],
  'as beforeRequest' => [
    'class' => 'yii\filters\AccessControl',
    'rules' => [
      [
        'allow' => true,
        'actions' => ['login'],
      ],
      [
        'allow' => true,
        'roles' => ['@'],
      ],
    ]
  ],
  'defaultRoute' => 'site/index',
  'params' => $params,
];
