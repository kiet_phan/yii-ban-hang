<?php
use yii\helpers\Url;
use yii\helpers\Html;
use backend\helper\CommonHelper;
?>
<div class="page-sidebar-wrapper">
  <div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
      <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
      <li class="sidebar-toggler-wrapper">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
      </li>
      <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
      <li class="sidebar-search-wrapper">
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
        <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
        <form class="sidebar-search " action="extra_search.html" method="POST">
          <a href="javascript:;" class="remove">
            <i class="icon-close"></i>
          </a>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
          </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['home'])?>" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
        <a href="<?=Url::toRoute(['home/index'])?>" >
          <i class="icon-home"></i>
					<span class="title">
					Home </span>
        </a>
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['employees'])?>" data-container="body" data-placement="right" data-html="true" data-original-title="">
        <a href="<?=Url::toRoute(['employees/index'])?>" >
          <i class="icon-basket"></i>
          <span class="title">Quản Trị  </span>
        </a>
      </li>
      <li class="<?=CommonHelper::getActiveMenu(['products','categories','banners'])?>">
        <a href="javascript:;">
          <i class="icon-basket"></i>
          <span class="title">eCommerce</span>
          <span class="arrow <?=CommonHelper::getActiveMenu(['products','categories','banners'])?>"></span>
        </a>
        <ul class="sub-menu">
          <li class="<?=CommonHelper::getActiveMenu(['products'])?>">
            <a href="<?=Url::toRoute(['products/index'])?>" >
              <i class="icon-home"></i>
              Sản Phẩm </a>
          </li>
<!--          <li>-->
<!--            <a href="ecommerce_orders.html">-->
<!--              <i class="icon-basket"></i>-->
<!--              Orders</a>-->
<!--          </li>-->
<!--          <li>-->
<!--            <a href="ecommerce_orders_view.html">-->
<!--              <i class="icon-tag"></i>-->
<!--              Order View</a>-->
          </li>
          <li class="<?=CommonHelper::getActiveMenu(['categories'])?>">
            <a href="<?=Url::toRoute(['categories/index'])?>" >
              <i class="icon-handbag"></i>
              Danh Mục Sản Phẩm </a>
          </li>

          <li class="<?=CommonHelper::getActiveMenu(['banners'])?>">
            <a href="<?=Url::toRoute(['banners/index'])?>" >
              <i class="icon-pencil"></i>
              Banner</a>
          </li>

        </ul>
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['customers'])?>" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
        <a href="<?=Url::toRoute(['customers/index'])?>" >
          <i class="icon-home"></i>
					<span class="title">
					Customer </span>
        </a>
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['transactions'])?>" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
        <a href="<?=Url::toRoute(['transactions/index'])?>" >
          <i class="icon-home"></i>
					<span class="title">
					Giao dịch </span>
        </a>
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['price-lists'])?>" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
        <a href="<?=Url::toRoute(['price-lists/index'])?>" >
          <i class="icon-home"></i>
					<span class="title">
					Bảng giá </span>
        </a>
      </li>
      <li class="tooltips <?=CommonHelper::getActiveMenu(['contacts'])?>">
        <a href="<?=Url::toRoute(['contacts/index'])?>" >
          <i class="icon-pencil"></i>
          Liên hệ</a>
      </li>


    </ul>
    <!-- END SIDEBAR MENU -->
  </div>
</div>
