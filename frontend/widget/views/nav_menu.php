<!-- Navbar -->
<?php
use frontend\helper\FrontEndHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\AppHelper;
use backend\helper\CommonHelper;

$yourCart = FrontEndHelper::printCart();
?>
<nav>
  <div class="container">
    <div class="row">
      <div class="nav-inner col-lg-12">
        <ul id="nav" class="hidden-xs">
          <li class="level0 parent drop-menu <?=CommonHelper::getActiveMenu(['home'])?>"><a href="<?= Url::toRoute('home/index') ?>"><span>Home</span></a>
          </li>
          <?= FrontEndHelper::generateMenu($categories) ?>
          <li class="level0 nav-8 level-top <?=CommonHelper::getActiveMenu(['contacts'])?>"><a href="<?= Url::toRoute('contacts/create') ?>" class="level-top"><span>Liên hệ</span></a></li>
          <li class="level0 nav-8 level-top <?=CommonHelper::getActiveMenu(['price-list'])?>"><a href="<?= Url::toRoute('price-list/index') ?>" class="level-top"><span>Bảng giá</span></a></li>
        </ul>
        <div class="menu_top">
          <div class="top-cart-contain pull-right">
            <!-- Top Cart -->
            <div class="mini-cart">
              <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"><a href="#"><span
                    class="hidden-xs">Shopping Cart (<?= count($yourCart) ?>)</span></a></div>
              <div>
                <div class="top-cart-content" style="display: none;">
                  <div class="block-subtitle">
                    <div class="top-subtotal"><?= count($yourCart) ?> items, <span
                        class="price"> <?= FrontEndHelper::getPriceCart($yourCart) ?> VND</span></div>
                    <!--top-subtotal-->
                    <div class="pull-right">
                      <a href="<?= Url::to(['cart/view']) ?>" class="view-cart"><span>View Car</span></a>
<!--                      <button title="View Cart" class="view-cart" type="button"><span>View Cart</span></button>-->
                    </div>
                    <!--pull-right-->
                  </div>
                  <!--block-subtitle-->
                  <ul class="mini-products-list" id="cart-sidebar">
                    <?php foreach ($yourCart as $cart): ?>
                      <?php $item = $cart['item'];
                      $images = $item->images;
                      ?>
                      <li class="item first">
                        <div class="item-inner">
                          <a class="product-image" title="<?= $item->name ?>" href="<?= Url::to(['products/view',"id"=>$item->id])?>">
                            <?= Html::img('@web/'.FrontEndHelper::imageUrl($images[0],$images[0]->file_path), [
                              'alt' => $item->name,
                              'class'=>''
                            ]) ?>
                          </a>
                          <div class="product-details">
                            <div class="access"><a class="btn-remove1" title="Remove This Item" href="<?= Url::to(['products/remove-cart',"id"=>$item->id])?>">Remove</a>
<!--                              <a class="btn-edit" title="Edit item" href="">-->
<!--                                <i class="icon-pencil"></i>-->
<!--                                <span class="hidden">Edit item</span>-->
<!--                              </a>-->
                            </div>
                            <!--access--> <strong><?= $cart['count']?></strong> x <span class="price"><?= AppHelper::formatPrice($item->price) ?> VND</span>
                            <p class="product-name"><a href="<?= Url::to(['products/view',"id"=>$item->id])?>"><?= $item->name ?>t</a></p>
                          </div>
                        </div>
                      </li>
                    <?php endforeach; ?>
                  </ul>
<!--                  <div class="actions">-->
<!--                    <button class="btn-checkout" title="Checkout" type="button"><span>Checkout</span></button>-->
<!--                  </div>-->
                  <!--actions-->
                </div>
              </div>
            </div>
            <!-- Top Cart -->
            <div id="ajaxconfig_info" style="display:none"><a href="#/"></a>
              <input value="" type="hidden">
              <input id="enable_module" value="1" type="hidden">
              <input class="effect_to_cart" value="1" type="hidden">
              <input class="title_shopping_cart" value="Go to shopping cart" type="hidden">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
<!-- end nav -->