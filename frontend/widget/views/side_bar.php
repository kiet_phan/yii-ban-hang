<?php
use yii\helpers\Html;
use frontend\helper\FrontEndHelper;
use yii\helpers\Url;
use common\helpers\AppHelper;
$yourCart = FrontEndHelper::printCart();

?>
<div class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
  <aside class="col-left sidebar">
    <div class="side-nav-categories">
      <div class="block-title"> Categories</div>
      <!--block-title-->
      <!-- BEGIN BOX-CATEGORY -->
      <div class="box-content box-category">
        <?= \frontend\helper\FrontEndHelper::generateSideBar($categories) ?>
      </div>
      <!--box-content box-category-->
    </div>

    <div class="block block-cart">
      <div class="block-title ">My Cart</div>
      <div class="block-content">
        <div class="summary">
          <p class="amount">Giỏ hàng của bạn có  <a href="#"> </a><?=count($yourCart)?>  sản phẩm</p>
          <p class="subtotal"><span class="label">Tổng số tiền:</span> <span class="price"> <?=FrontEndHelper::getPriceCart($yourCart)?> VND</span></p>
        </div>
        <div class="ajax-checkout">
          <a href="<?= Url::to(['products/view-cart']) ?>" class="button button-checkout"><span>View Car</span></a>
        </div>
        <p class="block-subtitle">Sản phẩm trong giỏ hàng: </p>
        <ul>
          <?php foreach($yourCart as $cart): ?>
            <?php $item = $cart['item'];
            $images = $item->images;
            ?>
          <li class="item"><a href="<?= Url::to(['products/view',"id"=>$item->id])?>" title="<?= $item->name ?>" class="product-image">
              <?= Html::img('@web/'.FrontEndHelper::imageUrl($images[0],$images[0]->file_path), [
                'alt' => $item->name,
                'class'=>''
              ]) ?>
            </a>
            <div class="product-details">
              <div class="access">
                <a href="<?= Url::to(['products/remove-cart',"id"=>$item->id])?>" title="Remove This Item" class="btn-remove1"> <span
                    class="icon"></span> Remove </a>
              </div>
              <p class="product-name">
                <a href="<?= Url::to(['products/view',"id"=>$item->id])?>"><?= $item->name ?></a>
              </p>
              <strong><?= $cart['count']?></strong> x <span class="price"><?= AppHelper::formatPrice($item->price) ?>VND </span></div>
          </li>
          <?php endforeach;?>
        </ul>
      </div>
    </div>


<!--    <div class="block block-tags">-->
<!--      <div class="block-title"> Popular Tags</div>-->
<!--      <div class="block-content">-->
<!--        <ul class="tags-list">-->
<!--          <li><a href="#" style="font-size:98.3333333333%;">Camera</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">Hohoho</a></li>-->
<!--          <li><a href="#" style="font-size:145%;">SEXY</a></li>-->
<!--          <li><a href="#" style="font-size:75%;">Tag</a></li>-->
<!--          <li><a href="#" style="font-size:110%;">Test</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">bones</a></li>-->
<!--          <li><a href="#" style="font-size:110%;">cool</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">cool t-shirt</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">crap</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">good</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">green</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">hip</a></li>-->
<!--          <li><a href="#" style="font-size:75%;">laptop</a></li>-->
<!--          <li><a href="#" style="font-size:75%;">mobile</a></li>-->
<!--          <li><a href="#" style="font-size:75%;">nice</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">phone</a></li>-->
<!--          <li><a href="#" style="font-size:98.3333333333%;">red</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">tight</a></li>-->
<!--          <li><a href="#" style="font-size:75%;">trendy</a></li>-->
<!--          <li><a href="#" style="font-size:86.6666666667%;">young</a></li>-->
<!--        </ul>-->
<!--        <div class="actions"><a href="#" class="view-all">View All Tags</a></div>-->
<!--      </div>-->
<!--    </div>-->
  </aside>
</div>