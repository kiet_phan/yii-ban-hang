<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\AppHelper;
?>
<section class="footer-navbar">
    <div class="footer-inner">

    </div>
    <div class="footer-middle">
      <div class="container">
        <div class="row">

          <address>
            <i class="icon-location-arrow"></i>Cơ sở 1: 218/46 Minh Phụng, Phường 6, Quận 6, TP Hồ Chí Minh
            <i class="icon-location-arrow"></i>Cơ sở 2: 768 Quốc Lộ 1A, P. Bình Hưng Hòa, Quận Bình Tân HCM <br>
            <i class="icon-mobile-phone"></i><span> 0901 787939 (Hải) hoặc 090 547 6024 (Ngân)</span>
            <i class="icon-envelope"></i><a href="mailto:hanahouse.clothing@gmail.com">hanahouse.clothing@gmail.com</a>
          </address>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-xs-12 coppyright">&copy; 2015 Magikcommerce. All Rights Reserved.</div>
         <!--  <div class="col-sm-7 col-xs-12 company-links">
            <ul class="links">
              <li><a title="Magento Themes" href="#">Magento Themes</a></li>
              <li><a title="Premium Themes" href="#">Premium Themes</a></li>
              <li><a title="Responsive Themes" href="#">Responsive Themes</a></li>
              <li class="last"><a title="Magento Extensions" href="#">Magento Extensions</a></li>
            </ul>
          </div> -->
        </div>
      </div>
    </div>
  </section>
