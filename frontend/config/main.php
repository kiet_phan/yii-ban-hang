<?php
$params = array_merge(
  require(__DIR__ . '/../../common/config/params.php'),
  require(__DIR__ . '/../../common/config/params-local.php'),
  require(__DIR__ . '/params.php'),
  require(__DIR__ . '/params-local.php')
);
return [
  'id' => 'app-frontend',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'frontend\controllers',
  'components' => [
    'user' => [
      'identityClass' => 'common\models\Customers',
      'enableAutoLogin' => false,
      'loginUrl' => [ 'customers/login' ],
      'identityCookie' => [
        'name' => '_frontendUser', // unique for backend
        'path'=>dirname(__DIR__).'/web'  // correct path for the backend app.
      ]
    ],
    'session' => [
      'name' => '_frontendSessionId', // unique for frontend
      'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'class' => 'yii\web\UrlManager',
      'baseUrl' => FRONEND_URL,
        'rules' => [
          '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
          'san-pham/<id:\d+><slug:[-a-zA-Z0-9]+>' => 'products/view',
          'danh-muc-san-pham/<id:\d+><slug:[-a-zA-Z0-9]+>' => 'categories/view'
        ],
    ],
  ],
  'as beforeRequest' => [
    'class' => 'yii\filters\AccessControl',
    'rules' => [
      [
        'allow' => true,
      ],
    ]
  ],
  'defaultRoute' => 'home/index',
  'params' => $params,
];
