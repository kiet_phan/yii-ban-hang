<?php
/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 5/2/16
 * Time: 10:19 AM
 */

namespace frontend\helper;

use yii\helpers\Url;
use yii\bootstrap\Html;
use Yii;
use common\helpers\AppHelper;

class FrontEndHelper
{
  public static function generateMenu($categories)
  {
    $tree = self::createTree($categories);
    $list = self::createList($tree, $categories, true);
    return $list;
  }
  public static function generateSideBar($categories)
  {
    $tree = self::createTree($categories);
    $list = self::createSideBarList($tree, $categories, true);
    return $list;
  }


  public static function createTree($categories, $lft = 1, $rgt = null)
  {
    $tree = [];
    foreach ($categories as $key => $cate) {
      if ($cate->lft == $lft + 1 && (is_null($rgt) || $cate->rgt < $rgt)) {
        $tree[$key] = self::createTree($categories, $cate->lft, $cate->rgt);
        $lft = $cate->rgt;
      }
    }
    return $tree;
  }

  public static function createList($keys, $categories, $parrent = null)
  {
    $level = key($keys);
    $dropmenu = '';
    if ($level === 1) {
      $dropmenu = "drop-menu";
      $list = '';
//      $list = "<li class=\"level0 parent drop-menu\"><a href=\"#\"><span>".$categories[$level]->name."</span></a><ul class=\"level1\">";
    } else {
      $list = "<ul class='level" . ($categories[$level]->depth - 1) . "' style=\"display: none;\">";
    }

    foreach ($keys as $key => $row) {
      $activeClass = self::getActiveMenu($categories[$key]->id);
      $product_url = Url::to(["danh-muc-san-pham/".$categories[$key]->id."-".$categories[$key]->slug]);
      if (count($row)) {
        $list .= "<li class='level" . ($categories[$key]->depth - 1) . " {$dropmenu} {$activeClass}'>" . "<a href='" . $product_url . "'><span>" . $categories[$key]->name . "</span></a> " . self::createList($row, $categories) . '</li>';
      } else {
        $list .= "<li class='level" . ($categories[$key]->depth - 1) . " {$activeClass}'>" . "<a href='" . $product_url . "'><span>" . $categories[$key]->name . "</span></a> " . '</li>';
      }
    }

    if ($level === 1) {
      $list .= "";
    } else {
      $list .= "</ul>";
    }
    return $list;
  }
  public static function createSideBarList($keys, $categories, $parrent = null){
    $level = key($keys);
    if ($level === 1) {
      $list = "<ul>";
    } else {
      $list = "<ul class='level" . ($categories[$level]->depth - 2) . "' style=\"display: none;\">";
    }

    foreach ($keys as $key => $row) {
      $product_url = Url::to(["danh-muc-san-pham/".$categories[$key]->id."-".$categories[$key]->slug]);
      if (count($row)) {
        $list .= "<li><a class=\"active\" href='".$product_url."'>".$categories[$key]->name."</a> <span class=\"subDropdown minus\"></span>";
        $list .= self::createList($row, $categories) . '</li>';
      } else {
        $list .= "<li><a href='".$product_url."'>".$categories[$key]->name." </a></li>";
      }
    }

    $list .= "</ul>";
    return $list;
  }

  public static function imageUrl($model, $image_name)
  {
    $className = $model->formName();
    $foderName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    $image_path = 'uploads/img/uploads/' . $foderName . DS . $model->id . DS . $image_name;
    return $image_path;
  }
  public static function fileUrl($model, $file_name)
  {
    $className = $model->formName();
    $foderName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    $file_path = Yii::$app->request->baseUrl .'/uploads/' . $foderName . DS . $model->id . DS . $file_name;
    return $file_path;
  }
  public static function check_current(){

  }
  public static function printCart(){
    $session = Yii::$app->session;
    $yourCart =[];
    if(!empty($session['cart'])){
      $yourCart = $session['cart'];
    }

    return $yourCart;

  }
  public static function getPriceCart($yourCart = []){
    $totalPrice = 0;
    if(count($yourCart)> 0){

      foreach($yourCart as $key=>$value){
        $totalPrice += ($value['count'] * ($value['item']->price- $value['item']->discount) );
      }
    }
    return AppHelper::formatPrice($totalPrice);
  }
  public static function optionSize(){
    return [
      ['']
    ];
  }
  public static function getActiveMenu($id){
    $request = Yii::$app->request;
    $currentController = \Yii::$app->controller->id;
    $category_id = $request->get('id');
    $class = '';
    if($currentController == 'categories'&& $id == intval($category_id)){
      $class = 'active';
    }
    return $class;
  }

}