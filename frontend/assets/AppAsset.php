<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'css/bootstrap.min.css',
    ['css/animate.css',"media"=> "all"],
    ['css/font-awesome.css',"media"=> "all"],
    'css/revslider.css',
    'css/owl.carousel.css',
    'css/owl.theme.css',
    'css/jquery.mobile-menu.css',
    'css/jquery.bxslider.css',
    ['css/style.css',"media"=> "all"],
    'css/home.css',
  ];
  public $js = [
    "js/bootstrap.min.js",
    "js/parallax.js",
    "js/revslider.js",
    "js/common.js",
    "js/owl.carousel.min.js",
    "js/jquery.mobile-menu.min.js",
    "js/jquery.bxslider.min.js",
    'js/jquery.flexslider.js',
    'js/product.js',
    "js/jquery-ui/jquery-ui-1.10.3.custom.min.js"
  ];
  public $depends = [
    'yii\web\JqueryAsset',
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
}