<?php

namespace frontend\controllers;
use common\models\Categories;
use common\models\Products;
use yii\data\Pagination;


class CategoriesController extends AppController
{
  public function actionIndex()
  {
    return $this->render('index');
  }
  public function actionView($id)
  {
    $query = Products::find()->where(['category_id'=>$id])->orderBy('created_at desc');
    $countQuery = clone $query;
    $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>12]);
    $products = $query->offset($pages->offset)
      ->limit($pages->limit)
      ->all();
    $category = Categories::findOne($id);
    return $this->render('view',['products'=>$products,'pages'=>$pages,'category'=>$category]);
  }


}
