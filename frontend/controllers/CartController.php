<?php
/**
 * Created by PhpStorm.
 * User: phan-kiet
 * Date: 5/19/16
 * Time: 10:24 PM
 */

namespace frontend\controllers;


use common\models\Orders;
use common\models\Products;
use common\models\Transactions;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\db\Transaction;
use yii\db\Query;


class CartController extends AppController
{
  public function actionCheckout()
  {
    $tran = new Transactions();

    if (Yii::$app->request->post()) {
      $session = Yii::$app->session;
      $data_carts = $session['cart'];
      $transaction_data = isset(Yii::$app->request->post()['Transactions']) ? Yii::$app->request->post()['Transactions'] : [];
      $connection = \Yii::$app->db;
      $transaction = $connection->beginTransaction();
      try {
        $detailCarts = $this->getDetailCart($data_carts);
        $tran->first_name = $transaction_data['first_name'];
        if($this->curent_user){
          $tran->customer_id = $this->curent_user->id;
        }
        $tran->province = $transaction_data['province'];
        $tran->user_email = $transaction_data['user_email'];
        $tran->last_name = $transaction_data['last_name'];
        $tran->district = $transaction_data['district'];
        $tran->user_phone = $transaction_data['user_phone'];
        $tran->address = $transaction_data['address'];
        $tran->amount = $detailCarts['amount'];

        if($tran->save()){
          foreach ($detailCarts['product'] as $product) {
            if (intval($data_carts[$product->id]['count']) > 0) {
              $order = new Orders();
              $order->product_id =  $product->id;
              $order->qty =  $data_carts[$product->id]['count'];
              $order->unit_price =  $product->price;
              $order->discount =  $product->discount;
              $order->size =  $data_carts[$product->id]['size'];
              $order->note =  $data_carts[$product->id]['note'];
              $order->discount =  $product->discount;
              $order->amount =   $product->price - $product->discount;
              $tran->link('orders', $order);
            }

          }
          $session->remove('cart');
        }
        $transaction->commit();

        return $this->redirect(['message']);
      } catch (\Exception $e) {
        $transaction->rollBack();
        throw $e;
      }
    }else{
      return $this->render('checkout',['model'=>$tran]);
    }

  }
  public function actionView(){
    return $this->render('view');
  }
  public function actionUpdateCart()
  {
    if(Yii::$app->request->post() && isset(Yii::$app->request->post()['cart'])){
      $carts = Yii::$app->request->post()['cart'];
      $session = Yii::$app->session;
      $temp_cart = $session['cart'];
      foreach(Yii::$app->request->post()['cart'] as $key => $cart){
        $temp_cart[intval($key)]['count'] = intval($cart['qty']);
        $temp_cart[intval($key)]['size'] = $cart['size'];
        $temp_cart[intval($key)]['note'] = $cart['note'];
      }
      $session['cart'] = $temp_cart;

    }
    return $this->redirect(Yii::$app->request->referrer);

  }
  public function actionMessage(){
    return $this->render('message');
  }

  private function getDetailCart($data_carts)
  {
    $product_ids = array_keys($data_carts);
    $items = Products::findAll($product_ids);
    $amount = 0;
    foreach ($items as $product) {
      if (intval($data_carts[$product->id]['count']) > 0) {
        $amount += ($product->price - $product->discount) * intval($data_carts[$product->id]['count']);

      }
    }
    return ['amount' => $amount, 'product' => $items];

  }


}