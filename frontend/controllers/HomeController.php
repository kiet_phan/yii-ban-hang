<?php

namespace frontend\controllers;

use common\models\Products;
use common\models\Categories;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;

class HomeController extends AppController
{
  public $layout = 'home_layout';
  public function behaviors()
  {
    return [

    ];
  }

  public function actionIndex()
  {
    $categories = Categories::find()->orderBy('lft')->all();
    $best_seller = Products::find()->where(["status"=>Products::STATUS_ACTIVE])->orderBy('purchased_quantity')->limit(12)->all();
    $new_product = Products::find()->orderBy("created_at desc")->limit(12)->all();
   
    return $this->render('index',[
      'categories'=>$categories,
      'best_seller'=>$best_seller,
      'new_product'=>$new_product
    ]);
  }

}
