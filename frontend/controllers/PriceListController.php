<?php

namespace frontend\controllers;

use common\models\PriceList;

class PriceListController extends AppController
{
  public function actionIndex()
  {
    $pdf_file = PriceList::find()->orderBy('created_at desc')->one();
    return $this->render('index',['pdf_file'=>$pdf_file]);
  }

}
