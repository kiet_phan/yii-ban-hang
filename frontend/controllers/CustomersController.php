<?php

namespace frontend\controllers;

use Yii;
use common\models\Customers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\LoginForm;
use yii\filters\AccessControl;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends AppController
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['login', 'logout','get-user'],
        'rules' => [
          [
            'allow' => true,
            'actions' => ['login'],
            'roles' => ['?'],
          ],
          [
            'allow' => true,
            'actions' => ['get-user','logout'],
            'roles' => ['@'],
          ],
        ],
      ]
    ];
  }


  /**
   * Displays a single Customers model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Customers model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionRegister(){
    $model = new Customers();
    return $this->render('register',[
      'model'=>$model
    ]);
  }
  public function actionCreate()
  {
    $model = new Customers();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      Yii::$app->user->switchIdentity($model);
      return $this->redirect(['home/index']);
    } else {
      return $this->render('register', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Customers model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
//    $model = $this->findModel($id);
//
//    if ($model->load(Yii::$app->request->post()) && $model->save()) {
//      return $this->redirect(['view', 'id' => $model->id]);
//    } else {
//      return $this->render('update', [
//        'model' => $model,
//      ]);
//    }
  }
  public function actionLogin(){
    $this->layout = 'home_layout';
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->redirect(['home/index']);
    } else {
      return $this->render('login', [
        'model' => $model,
      ]);
    }
  }
  public function actionGetUser(){
    $customer = Yii::$app->user->identity;
    $response = Yii::$app->response;
    $response->format = \yii\web\Response::FORMAT_JSON;
    $response->data = ['customer'=>$customer->toArray()];

    return $response;
  }


  /**
   * Finds the Customers model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Customers the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Customers::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }
}
