<?php

namespace frontend\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use common\models\Products;

class ProductsController extends AppController
{

  public function actionIndex($category_id = null)
  {

    return $this->render('index');
  }
  public function actionView($id)
  {
    $product = $this->findModel($id);
    $relation_products = Products::find()->where(['category_id'=>$product->category_id,"status"=> Products::STATUS_ACTIVE])
      ->where(['<>',"id", $product->id])->orderBy("created_at desc")->limit(10)->all();
    return $this->render('view',[
      'product'=> $product,
      'relation_products'=>$relation_products
    ]);
  }
  public function actionAddToCart($id){
    $request = Yii::$app->request;
    $session = Yii::$app->session;
    $product = $this->findModel($id);
    $image =$product->images[0];
    $qty = intval($request->get('qty',1));
    $carts = [];;
    if(isset($session['cart'])){
      $carts = $session['cart'];
      if(empty($carts[$product->id])){
        $carts[$product->id] = ["item"=>$product,'count'=> $qty,'image'=>$image,'size'=>'','note'=>''];
      }else{
        $carts[$product->id]['count'] = $carts[$product->id]['count'] + 1;
      }
      $session['cart'] = $carts;
    }else{
      $carts[$product->id] = ["item"=>$product,'count'=> $qty,'image'=>$image,'size'=>'','note'=>''];
      $session['cart'] = $carts;
    }
    return $this->redirect(Yii::$app->request->referrer);
//    $response = Yii::$app->response;
//    $response->format = \yii\web\Response::FORMAT_JSON;
//    $response->data = $carts;
//
//    return $response;
  }
  public function actionViewCart(){
    return $this->render('view_cart');
  }

  public function actionClearCart(){
    $session = Yii::$app->session;
    $session->remove('cart');
    return $this->redirect(Url::home());
  }
  public function actionRemoveCart($id){
    if($id){
      $session = Yii::$app->session;
      $temp_cart = $session['cart'];
      unset($temp_cart[intval($id)]);
      $session['cart'] = $temp_cart;
    }
    return $this->redirect(Yii::$app->request->referrer);

  }

  protected function findModel($id)
  {
    if (($model = Products::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
