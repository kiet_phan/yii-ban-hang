<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helper\FrontEndHelper;
use common\helpers\AppHelper;
?>
<div class="related-slider col-lg-12 col-xs-12 bounceInDown animated">
  <div class="slider-items-products">
    <div class="slider-items-products">
      <div class="new_title center">
        <h2>Related Products</h2>
      </div>

      <div id="related-products-slider" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid">
          <?php foreach($relation_products as $item): ?>
            <?php
            $images = $item->images;
            if(count($images)):
              ?>
          <div class="item">
            <div class="item-inner">
              <div class="item-img">
                <div class="item-img-info"> <a class="product-image"  href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug])?>" title="<?=$item->name?>">  <?= Html::img('@web/'.FrontEndHelper::imageUrl($images[0],$images[0]->file_path), [
                      'alt' => $item->name,
                      'class'=>''
                    ]) ?></a>
                  <div class="sale-label sale-top-left">sale</div>
                  <div class="item-box-hover">
                    <div class="box-inner">
                      <div class="actions">
                        <div class="add_cart">
                          <button data-id="<?=$item->id?>" class="button btn-cart" type="button"><span>Add to Cart</span></button>
                        </div>
                        <div class="product-detail-bnt">
                          <a href="#" class="button detail-bnt"><span>Quick View</span></a>
                        </div>
<!--                        <span class="add-to-links">-->
<!--                          <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>-->
<!--                          -->
<!--                        </span> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item-info">
                <div class="info-inner">
                  <div class="item-title"> <a title="<?=$item->name?>" href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug])?>"> <?=$item->name?> </a> </div>
                  <div class="item-content">
                    <div class="rating">
                      <div class="ratings">
                        <div class="rating-box">
                          <div style="width:80%" class="rating"></div>
                        </div>
                        <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                      </div>
                    </div>
                    <div class="item-price">
                      <div class="price-box"> <span  class="regular-price"> <span class="price">VND <?= AppHelper::formatPrice($item->price) ?></span> </span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>
          <?php endforeach; ?>


        </div>
      </div>

    </div>
  </div>
</div>