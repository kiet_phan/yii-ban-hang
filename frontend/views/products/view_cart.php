<?php
use frontend\helper\FrontEndHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\AppHelper;
$yourCart = FrontEndHelper::printCart();
?>
<section class="main-container col1-layout wow bounceInUp animated">
  <div class="main container">
    <div class="col-main">
      <form method="post" action="<?= Url::to(['cart/checkout']) ?>">
        <div class="cart">
          <div class="page-title">
            <h2>Shopping Cart</h2>
          </div>
          <div class="table-responsive">

            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <fieldset>
              <table class="data-table cart-table" id="shopping-cart-table">
                <thead>
                <tr class="first last">
                  <th rowspan="1">&nbsp;</th>
                  <th rowspan="1"><span class="nobr">Tên sản phẩm</span></th>
                  <th rowspan="1" class="hidden-phone"></th>
                  <th colspan="1" class="a-center"><span class="nobr">Giá gốc</span></th>
                  <th rowspan="1" class="hidden-phone"><span class="nobr">Giảm giá</span></th>
                  <th class="a-center " rowspan="1">Số lượng</th>
                  <th colspan="1" class="a-center">Thành tiền</th>
                  <th class="a-center" rowspan="1">&nbsp;</th>
                </tr>
                </thead>
                <tfoot>
                <tr class="first last">
                  <td class="a-right last" colspan="8">
                    <button onclick="setLocation('<?= Url::to(['home/index']) ?>')"
                            class="button btn-continue" title="Continue Shopping" type="button">
                      <span>Continue Shopping</span></button>
                    <button onclick="window.location.href = baseUrl + '/products/clear-cart'" id="empty_cart_button"
                            class="button" title="Clear Cart" type="button"><span>Clear Cart</span></button>
                  </td>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($yourCart as $cart): ?>
                  <?php $item = $cart['item'];
                  $images = $item->images;
                  ?>
                  <tr class="first odd">
                    <td class="image">
                      <a href="<?= Url::to(['products/view', "id" => $item->id]) ?>" title="<?= $item->name ?>"
                         class="product-image">
                        <?= Html::img('@web/' . FrontEndHelper::imageUrl($images[0], $images[0]->file_path), [
                          'alt' => $item->name,
                          'width' => "75"
                        ]) ?>
                      </a>
                    </td>
                    <td><h2 class="product-name"><a
                          href="<?= Url::to(['products/view', "id" => $item->id]) ?>"><?= $item->name ?></a></h2></td>
                    <td class="a-center hidden-table"><a title="Edit item parameters" class="edit-bnt" href="#"></a>
                    </td>
                    <td class="a-center hidden-table"><a class="link-wishlist1 use-ajax"
                                                         href="#">$<?= AppHelper::formatPrice($item->price) ?></a></td>
                    <td class="a-center hidden-table"><span class="cart-price"> <span
                          class="price">$<?= AppHelper::formatPrice($item->discount) ?></span> </span></td>
                    <td class="a-center movewishlist">
                      <input maxlength="12" class="input-text qty" title="Qty" size="4" value="<?= $cart['count'] ?>"
                             name="cart[<?= $item->id ?>][qty]">
                    </td>
                    <td class="a-center movewishlist"><span class="cart-price"> <span
                          class="price">$<?= AppHelper::formatPrice($cart['count']*($item->price - $item->discount))?></span> </span></td>
                    <td class="a-center last"><a class="button remove-item" title="Remove item"
                                                 href="<?= Url::to(['products/remove-cart', "id" => $item->id]) ?>"><span><span>Remove item</span></span></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </fieldset>
          </div>
          <!-- BEGIN CART COLLATERALS -->
          <div class="cart-collaterals row">
            <div class="col-sm-8">
              <div class="shipping">
                <h3>Thông tin khách hàng</h3>
                <div class="shipping-form">
                  <div class="col-sm-6">
                    <ul class="form-list">
                      <li>
                        <label for="postcode">Họ</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[first_name]" class="input-text validate-postcode">
                        </div>
                      </li>
                      <li>
                        <label for="postcode">Tỉnh/Thành phố</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[province]" class="input-text validate-postcode">
                        </div>
                      </li>
                      <li>
                        <label for="postcode">Email</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[user_email]" class="input-text validate-postcode">
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="form-list">
                      <li>
                        <label for="postcode">Tên</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[last_name]" class="input-text validate-postcode">
                        </div>
                      </li>
                      <li>
                        <label for="postcode">Quận/Huyện</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[district]" class="input-text validate-postcode">
                        </div>
                      </li>
                      <li>
                        <label for="postcode">Số điện thoại</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[user_phone]" class="input-text validate-postcode">
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-12">
                    <ul class="form-list">
                      <li>
                        <label for="postcode">Địa chỉ</label>
                        <div class="input-box">
                          <input type="text" name="Transactions[address]" id="postcode"
                                 class="input-text validate-postcode">
                        </div>
                      </li>

                    </ul>
                  </div>


                </div>
              </div>
            </div>
            <div class="totals col-sm-4">
              <h3>Tổng giá trị đơn hàng</h3>
              <div class="inner">
                <table class="table shopping-cart-table-total" id="shopping-cart-totals-table">

                  <tfoot>
                  <tr>
                    <td colspan="1" class="a-left"><strong>Thành tiền</strong></td>
                    <td class="a-right"><strong><span class="price">$<?=FrontEndHelper::getPriceCart($yourCart)?></span></strong></td>
                  </tr>
                  </tfoot>
                </table>
                <ul class="checkout">
                  <li>
                    <button class="button btn-proceed-checkout" title="Proceed to Checkout" type="submit"><span>Đặt hàng</span>
                    </button>
                  </li>

                </ul>
              </div>
              <!--inner-->

            </div>
          </div>

          <!--cart-collaterals-->

        </div>

      </form>
    </div>
  </div>
</section>