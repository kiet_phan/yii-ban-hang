<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helper\FrontEndHelper;
use common\helpers\AppHelper;
$images = $product->images;

?>
<div class="main-container col1-layout">
  <div class="main container">
    <div class="col-main">
      <div class="row">
        <div class="product-view">
          <div class="product-essential">
            <form action="#" method="post" id="product_addtocart_form">
              <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
              <div class="product-img-box col-sm-5 col-xs-12 bounceInRight animated">
                <?php if($product->isNew()): ?>
                  <div class="new-label new-top-left"> New </div>
                <?php endif; ?>
                <div class="product-image">
                  <div class="large-image"> <a href="<?=Yii::getAlias('@web').DS.FrontEndHelper::imageUrl($images[0],$images[0]->file_path)?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20">
                      <?= Html::img('@web/'.FrontEndHelper::imageUrl($images[0],$images[0]->file_path), [
                        'alt' => $product->name,
                        'class'=>''
                      ]) ?>
                       </a> </div>
                  <div class="flexslider flexslider-thumb">
                    <ul class="previews-list slides">
                      <?php $i = 1?>
                      <?php foreach($images as $image):?>
                      <li>
                        <a href='<?=Yii::getAlias('@web').DS.FrontEndHelper::imageUrl($image,$image->file_path)?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=Yii::getAlias('@web').DS.FrontEndHelper::imageUrl($image,$image->file_path)?>' ">
                          <?= Html::img('@web/'.FrontEndHelper::imageUrl($image,$image->file_path), [
                            'alt' => $product->name,
                            'class'=>''
                          ]) ?>
                        </a>
                      </li>
                        <?php $i++?>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                </div>
                <!-- end: more-images -->
              </div>
              <div class="product-shop col-sm-7 col-xs-12 bounceInUp animated">
                <div class="product-next-prev"> <a class="product-next" href="#"><span></span></a> <a class="product-prev" href="#"><span></span></a> </div>
                <div class="product-name">
                  <h1><?=$product->name?></h1>
                </div>
                <div class="short-description">
                  <?=$product->summary ?>
                </div>
<!--                <div class="ratings">-->
<!--                  <div class="rating-box">-->
<!--                    <div style="width:60%" class="rating"></div>-->
<!--                  </div>-->
<!--                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>-->
<!--                </div>-->
                <div class="price-block">
                  <div class="price-box">
<!--                    <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $315.99 </span> </p>-->
                    <p class="special-price">
                      <span class="price-label">Special Price</span>
                      <span id="product-price-48" class="price"> VND <?= AppHelper::formatPrice($product->price)?> </span>
                    </p>
                  </div>
                </div>
                <div class="add-to-box">
                  <div class="add-to-cart">
                    <label for="qty">Số lượng:</label>
                    <div class="pull-left">
                      <div class="custom pull-left">
                        <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="icon-plus">&nbsp;</i></button>
                        <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">

                        <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="icon-minus">&nbsp;</i></button>
                      </div>
                    </div>

                    <button data-id="<?= $product->id ?>" class="button btn-cart" title="Add to Cart" type="button"><span><i class="icon-basket"></i> Thêm vào giỏ hàng</span></button>

                  </div>
                </div>
              </div>

            </form>
          </div>
          <div class="product-collateral col-sm-12 col-xs-12 bounceInUp animated">
            <div class="add_info">
              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                <li class="active"> <a href="#product_tabs_description" data-toggle="tab"> Mô tả chi tiết </a> </li>
<!--                <li><a href="#product_tabs_tags" data-toggle="tab">Tags</a></li>-->
<!--                <li> <a href="#reviews_tabs" data-toggle="tab">Reviews</a> </li>-->
              </ul>
              <div id="productTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="product_tabs_description">
                  <div class="std"><?= $product->content ?></div>
                </div>
<!--                <div class="tab-pane fade" id="product_tabs_tags">-->
<!--                  <div class="box-collateral box-tags">-->
<!--                    <div class="tags">-->
<!--                      <form id="addTagForm" action="#" method="get">-->
<!--                        <div class="form-add-tags">-->
<!--                          <label for="productTagName">Add Tags:</label>-->
<!--                          <div class="input-box">-->
<!--                            <input class="input-text" name="productTagName" id="productTagName" type="text">-->
<!--                            <button type="button" title="Add Tags" class=" button btn-add" onClick="submitTagForm()"> <span>Add Tags</span> </button>-->
<!--                          </div>-->
<!--                          <!--input-box-->
<!--                        </div>-->
<!--                      </form>-->
<!--                    </div>-->
<!--                    <!--tags-->
<!--                    <p class="note">Use spaces to separate tags. Use single quotes (') for phrases.</p>-->
<!--                  </div>-->
<!--                </div>-->

              </div>
            </div>
          </div>
          <?= $this->render('_relation_product', [
            'relation_products'=>$relation_products
          ]) ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->registerJsFile('@web/js/cloud-zoom.js', ['depends' => [frontend\assets\AppAsset::className()]])?>