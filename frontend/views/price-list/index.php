<?php
use frontend\helper\FrontEndHelper;
?>

<div class="main-container col2-right-layout wow bounceInUp animated">
  <div class="main container">
    <div class="row">
      <div class="col-main col-sm-9">
        <div class="my-account">
          <div class="page-title">
            <h2>Bảng giá</h2>
          </div>
          <div class="dashboard">
            <div class="welcome-msg"><strong>Bảng giá sỉ và lẻ</strong>
              <p>Xem chi tiết bảng giá sỉ lẻ tại hana house, quý khách vui lòng nhấp vào link sau</p>
              <a style="color: #00CC00" href="<?=FrontEndHelper::fileUrl($pdf_file,$pdf_file->file_name)?>" target="_blank"> Bảng giá sỉ và lẻ</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>