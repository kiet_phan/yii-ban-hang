<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */

?>
<section class="main-container col1-layout wow bounceInUp animated">
  <div class="main container">
    <div class="account-login">
      <div class="page-title">
        <h2>Đăng nhập hoặc tạo tài khoản</h2>
      </div>
      <fieldset class="col2-set">
        <legend>Đăng nhập hoặc tạo tài khoản</legend>
        <div class="col-1 new-users"><strong>Tạo tài khoản</strong>
          <div class="content">
<!--            <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>-->
            <div class="buttons-set">
              <button class="button create-account"><span>Tạo tài khoản</span></button>
            </div>
          </div>
        </div>
        <?php $form = ActiveForm::begin(
          [
            'fieldConfig' => [
              'options' => [
                'tag'=>false
              ],
            ]
          ]
        ); ?>
        <div class="col-2 registered-users">
          <div class="content">
            <ul class="form-list">
              <li>
                <?= $form->field($model, 'email')
                  ->textInput(['maxlength' => true,'class' => 'input-text'])
                  ->label('Email'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']);
                ?>
              </li>
              <li>
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'class' => 'input-text'])
                  ->label('Mật khẩu'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
              </li>
            </ul>
            <p class="required">* Trường bắt buộc</p>
            <div class="buttons-set">
              <button id="send2" name="send" type="submit" class="button login"><span>Đăng nhập</span></button>
<!--              <a class="forgot-word" href="#">Forgot Your Password?</a> </div>-->
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </fieldset>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
  </div>
</section>