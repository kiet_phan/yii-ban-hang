<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(
  [
    'action'=> Url::to(['customers/create']),
    'fieldConfig' => [
      'options' => [
        'tag'=>'div',
        'class'=>'input-box register-customer'
      ],
    ]
  ]
); ?>
  <ul>
    <li>
      <div class="customer-name">
        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Tên'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Họ'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
      </div>
    </li>
    <li>
      <?= $form->field($model, 'email')
        ->textInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Email'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']);
      ?>
      <?= $form->field($model, 'phone')
        ->textInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Số điện thoại',['class'=>'label-class']); ?>
    </li>
    <li>
      <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Mật khẩu'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
      <?= $form->field($model, 'password_confirm')->passwordInput(['maxlength' => true,'class' => 'input-text'])->label('Xác nhận mật khẩu'. Html::tag('span', '*',['class'=>'required']). Html::tag('br'),['class'=>'label-class']); ?>
    </li>
    <li>
      <?= $form->field($model, 'province')->textInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Tỉnh/Thành phố'.Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
      <?= $form->field($model, 'district')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Huyện/Quận'. Html::tag('span', '*',['class'=>'required']). Html::tag('br'),['class'=>'label-class']); ?>
    </li>
    <li>
      <?= $form->field($model, 'address',['template' => '{label} <br> {input}{error}{hint}','options'=>['tag'=>false]])
        ->textInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Địa chỉ',['class'=>'label-class']);
      ?>

    </li>
    <li>
      <?= $form->field($model, 'ship_address',['template' => '{label} <br> {input}{error}{hint}','options'=>['tag'=>false]])
        ->textInput(['maxlength' => true,'class' => 'input-text'])
        ->label('Địa chỉ giao hàng',['class'=>'label-class']); ?>
    </li>
    <li><span class="require"><em class="required">* </em>Required Fields</span>
      <div class="buttons-set">
        <button type="submit" title="Submit" class="button submit"> <span> Submit </span> </button>
      </div></li>
  </ul>
<?php ActiveForm::end(); ?>