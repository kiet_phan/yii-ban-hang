<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="<?=Url::to(['home/index'])?>">Home</a><span>&mdash;›</span></li>
          <li class="category13"><strong>Tạo tài khoản</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<div class="main-container col2-right-layout wow bounceInUp animated">
  <div class="main container">
    <div class="row">
      <section class="col-main col-sm-9">
        <div class="page-title">
          <h2>Đăng ký tài khoản</h2>
        </div>
        <div class="static-contain">
          <fieldset class="group-select">
            <ul>
              <li id="billing-new-address-form">
                <fieldset>
                  <legend>Đăng ký tài khoản</legend>
                  <input type="hidden" name="billing[address_id]" id="billing:address_id">
                  <?= $this->render('_form', [
                    'model' => $model,
                  ]) ?>
                </fieldset>
              </li>
            </ul>
          </fieldset>
        </div>
      </section>
<!--      <aside class="col-right sidebar col-sm-3">-->
<!--        <div class="block block-company">-->
<!--          <div class="block-title">Company </div>-->
<!--          <div class="block-content">-->
<!--            <ol id="recently-viewed-items">-->
<!--              <li class="item odd"><a href="about_us.html">About Us</a></li>-->
<!---->
<!--              <li class="item  odd"><a href="#">Terms of Service</a></li>-->
<!--              <li class="item even"><a href="#">Search Terms</a></li>-->
<!--              <li class="item last"><strong>Contact Us</strong></li>-->
<!--            </ol>-->
<!--          </div>-->
<!--        </div>-->
<!--      </aside>-->
    </div>
  </div>
</div>
<!-- Main Container End -->
