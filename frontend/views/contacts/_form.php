<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\helper\FrontEndHelper;

$yourCart = FrontEndHelper::printCart();


/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(
  [
    'fieldConfig' => [
      'options' => [
        'tag'=>'div',
        'class'=>'input-box register-customer'
      ],
    ]
  ]
); ?>
<ul>
  <li>
    <div class="customer-name">
      <?= $form->field($model, 'name')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Tên'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
      <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Số điện thoại'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>
    </div>
  </li>
  <li>
    <?= $form->field($model, 'email',['options'=>['class'=>'']])
      ->textInput(['maxlength' => true,'class' => 'input-text'])
      ->label('Email'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']);
    ?>
  </li>
  <li>
    <?= $form->field($model, 'address',['options'=>['class'=>'']])
      ->textInput(['maxlength' => true,'class' => 'input-text'])
      ->label('Địa chỉ'. Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']);
    ?>
  </li>
  <li>
    <?= $form->field($model, 'comment',['options'=>['class'=>'']])->textarea(['maxlength' => true,'class' => 'input-text'])
      ->label('Nội dung'.Html::tag('span', '*',['class'=>'required']),['class'=>'label-class']); ?>

  </li>
  <li><span class="require"><em class="required">* </em>Required Fields</span>
    <div class="buttons-set">
      <button type="submit" title="Submit" class="button submit"> <span> Submit </span> </button>
    </div></li>
</ul>
<?php ActiveForm::end(); ?>
