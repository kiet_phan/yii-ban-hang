<!-- end nav --> <!-- Breadcrumbs -->
<?php
use yii\helpers\Url;
?>

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"><a title="Go to Home Page" href="<?=Url::home() ?>">Home</a><span>&mdash;›</span>
          </li>
          <li class="category13"><strong>Liên hệ</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<div class="main-container col2-right-layout wow bounceInUp animated">
  <div class="main container">
    <div class="row">
      <section class="col-main col-sm-12">
        <div class="page-title">
          <h2>Contact Us</h2>
        </div>
        <div class="static-contain">
          <fieldset class="group-select">
            <?= $this->render('_form', ['model' => $model]) ?>
          </fieldset>
        </div>
      </section>
    </div>
  </div>
</div>
<!-- Main Container End -->