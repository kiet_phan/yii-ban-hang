<?php
use frontend\widget\SideBarWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helper\FrontEndHelper;
use common\helpers\AppHelper;
use frontend\component\CustomPagination;
?>
<div class="breadcrumbs bounceInUp animated">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"><a title="Go to Home Page" href="<?= Url::to(['home/index']) ?>">Trang chủ</a><span>» </span></li>
          <li class=""><a title="Go to Home Page" href="#"><?= $category->name ?></a><span>» </span></li>
<!--          <li class="category13"><strong>Tops & Tees</strong></li>-->
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->

<section class="main-container col2-left-layout bounceInUp animated">
  <div class="page-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h2><?= $category->name ?></h2></div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-main col-sm-9 col-sm-push-3">
        <article class="col-main">


          <div class="toolbar">

            <div id="sort-by">
              <label class="left">Sort By: </label>
              <ul>
                <li><a href="#">Position<span class="right-arrow"></span></a>
                  <ul>
                    <li><a href="#">Name</a></li>
                    <li><a href="#">Price</a></li>
                    <li><a href="#">Position</a></li>
                  </ul>
                </li>
              </ul>
              <a class="button-asc left" href="#" title="Set Descending Direction"><span
                  class="glyphicon glyphicon-arrow-up"></span></a></div>
            <div class="pager">


<!--              <div id="limiter">-->
<!--                <label>View: </label>-->
<!--                <ul>-->
<!--                  <li><a href="#">15<span class="right-arrow"></span></a>-->
<!--                    <ul>-->
<!--                      <li><a href="#">20</a></li>-->
<!--                      <li><a href="#">30</a></li>-->
<!--                      <li><a href="#">35</a></li>-->
<!--                    </ul>-->
<!--                  </li>-->
<!--                </ul>-->
<!--              </div>-->
              <div class="pages">
                <label>Page:</label>
                <?php
                echo CustomPagination::widget([
                  'pagination' => $pages,
                ]);
                ?>
<!--                <ul class="pagination">-->
<!--                  <li><a href="#">&laquo;</a></li>-->
<!--                  <li class="active"><a href="#">1</a></li>-->
<!--                  <li><a href="#">2</a></li>-->
<!--                  <li><a href="#">3</a></li>-->
<!--                  <li><a href="#">4</a></li>-->
<!--                  <li><a href="#">5</a></li>-->
<!--                  <li><a href="#">&raquo;</a></li>-->
<!--                </ul>-->
              </div>
            </div>
          </div>
          <div class="category-products">
            <ul class="products-grid">
              <?php foreach ($products as $item): ?>
                <?php
                $images = $item->images;
                if (count($images)):
                  ?>

                  <li class="item col-lg-4 col-md-3 col-sm-4 col-xs-6 item-category">
                    <div class="item-inner">
                      <div class="item-img">
                        <div class="item-img-info">
                          <a href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug]) ?>" title="<?= $item->name ?>"
                             class="product-image">
                            <?= Html::img('@web/' . FrontEndHelper::imageUrl($images[0], $images[0]->file_path), [
                              'alt' => $item->name,
                              'class' => ''
                            ]) ?> </a>
                          <div class="new-label new-top-left">New</div>
                          <div class="item-box-hover">
                            <div class="box-inner">
                              <div class="actions">
                                <div class="add_cart">
                                  <button data-id="<?= $item->id ?>" class="button btn-cart" type="button"><span>Add to Cart</span>
                                  </button>
                                </div>
                                <div class="product-detail-bnt">
                                  <a href="#" class="button detail-bnt"><span>Quick View</span></a>
                                </div>
                                <span class="add-to-links">
        <!--                      <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a> -->
        <!--                      <a href="compare.html" class="link-compare add_to_compare" title="Add to Compare"><span>Add to Compare</span></a>-->
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title">
                            <a href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug]) ?>"
                               title="Sample Product"><?= $item->name ?></a></div>
                          <div class="item-content">
                            <div class="rating">
                              <div class="ratings">
                                <div class="rating-box">
                                  <div class="rating" style="width:80%"></div>
                                </div>
                                <p class="rating-links"><a href="#">1 Review(s)</a> <span class="separator">|</span> <a
                                    href="#">Add Review</a></p>
                              </div>
                            </div>
                            <div class="item-price">
                              <div class="price-box"><span class="regular-price">
                                  <span class="price">VND <?= AppHelper::formatPrice($item->price) ?></span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
        </article>
        <!--	///*///======    End article  ========= //*/// -->
      </div>
      <?= SideBarWidget::widget() ?>
    </div>
  </div>
</section>
