<?php
use frontend\helper\FrontEndHelper;
?>

<div class="main-container col2-right-layout wow bounceInUp animated">
  <div class="main container">
    <div class="row">
      <div class="col-main col-sm-9">
        <div class="my-account">
          <div class="page-title">
            <h2>Đặt hàng thành công</h2>
          </div>
          <div class="dashboard">
            <div class="welcome-msg">
              <p>Cảm ơn bạn đã mua hàng tại shop của chúng tôi. Chúng tôi sẽ giao hàng cho bạn sớm nhất có thể</p>
              <a style="color: #00CC00" href="<?=\yii\helpers\Url::home() ?>" > Quay về trang chủ</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>