<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\helper\FrontEndHelper;
$yourCart = FrontEndHelper::printCart();


/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(
  [
    'fieldConfig' => [
      'options' => [
        'tag'=>'div',
        'class'=>'input-box'
      ],
    ]
  ]
); ?>

    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
    <!-- BEGIN CART COLLATERALS -->
    <div class="cart-collaterals row">
      <div class="col-sm-8">
        <div class="shipping">
          <h3>Thông tin khách hàng</h3>
          <div class="col-sm-12">
            <input type="checkbox" value="1" id="use_customer_info"> Sử dụng thông tin bạn đã đăng ký
            <br>
          </div>
          <div class="shipping-form">

            <div class="col-sm-6">

              <ul class="form-list">
                <li>
                  <?= $form->field($model, 'first_name')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Tên'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
<!--                  <label for="postcode">Họ</label>-->
<!--                  <div class="input-box">-->
<!--                    <input type="text" name="Transactions[first_name]" class="input-text validate-postcode">-->
<!--                  </div>-->
                </li>
                <li>
                  <?= $form->field($model, 'province')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Tỉnh/thành phố'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
                </li>
                <li>
                  <?= $form->field($model, 'user_email')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Email'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
                </li>
              </ul>
            </div>
            <div class="col-sm-6">
              <ul class="form-list">
                <li>
                  <?= $form->field($model, 'last_name')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Họ'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>

                </li>
                <li>
                  <?= $form->field($model, 'district')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Quận/Huyện'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
                </li>
                <li>
                  <?= $form->field($model, 'user_phone')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Điện thoại'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
                </li>
              </ul>
            </div>
            <div class="col-sm-12">
              <ul class="form-list">
                <li>
                  <?= $form->field($model, 'address')->textInput(['maxlength' => true,'class' => 'input-text'])->label('Địa chỉ'. Html::tag('span', '*',['class'=>'required']),['class'=>'']); ?>
                </li>

              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="totals col-sm-4">
        <h3>Tổng giá trị đơn hàng</h3>
        <div class="inner">
          <table class="table shopping-cart-table-total" id="shopping-cart-totals-table">

            <tfoot>
            <tr>
              <td colspan="1" class="a-left"><strong>Thành tiền</strong></td>
              <td class="a-right"><strong><span
                    class="price">$<?= FrontEndHelper::getPriceCart($yourCart) ?></span></strong></td>
            </tr>
            </tfoot>
          </table>
          <ul class="checkout">
            <li>
              <button class="button btn-proceed-checkout" title="Proceed to Checkout" type="submit">
                <span>Đặt hàng</span>
              </button>
            </li>

          </ul>
        </div>
        <!--inner-->

      </div>
    </div>

<?php ActiveForm::end(); ?>