<?php
use frontend\helper\FrontEndHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\AppHelper;

$yourCart = FrontEndHelper::printCart();
?>
<section class="main-container col1-layout wow bounceInUp animated">
  <div class="main container">
    <div class="col-main">

      <div class="cart">
        <div class="page-title">
          <h2>Xác nhận đơn hàng</h2>
        </div>
        <div class="table-responsive">


          <table class="data-table cart-table" id="shopping-cart-table">
            <thead>
            <tr class="first last">
              <th rowspan="1">&nbsp;</th>
              <th rowspan="1"><span class="nobr">Tên sản phẩm</span></th>
              <th rowspan="1" class="hidden-phone"></th>
              <th colspan="1" class="a-center"><span class="nobr">Giá gốc</span></th>
              <th rowspan="1" class="hidden-phone"><span class="nobr">Giảm giá</span></th>
              <th class="a-center " rowspan="1">Số lượng</th>
              <th colspan="1" class="a-center">Thành tiền</th>
            </tr>
            </thead>
            <tfoot>
            <tr class="first last">
              <td class="a-right last" colspan="8">
                <button onclick="setLocation('<?= Url::to(['home/index']) ?>')" class="button btn-continue" title="Continue Shopping" type="button">
                  <span>Continue Shopping</span>
                </button>
              </td>
            </tr>
            </tfoot>
            <tbody>
            <?php foreach ($yourCart as $cart): ?>
              <?php $item = $cart['item'];
              $images = $item->images;
              ?>
              <tr class="first odd">
                <td class="image">
                  <a href="<?= Url::to(['products/view', "id" => $item->id]) ?>" title="<?= $item->name ?>"
                     class="product-image">
                    <?= Html::img('@web/' . FrontEndHelper::imageUrl($images[0], $images[0]->file_path), [
                      'alt' => $item->name,
                      'width' => "75"
                    ]) ?>
                  </a>
                </td>
                <td><h2 class="product-name"><a
                      href="<?= Url::to(['products/view', "id" => $item->id]) ?>"><?= $item->name ?></a></h2></td>
                <td class="a-center hidden-table"><a title="Edit item parameters" class="edit-bnt" href="#"></a>
                </td>
                <td class="a-center hidden-table"><a class="link-wishlist1 use-ajax"
                                                     href="#">$<?= AppHelper::formatPrice($item->price) ?></a></td>
                <td class="a-center hidden-table"><span class="cart-price"> <span
                      class="price">$<?= AppHelper::formatPrice($item->discount) ?></span> </span></td>
                <td class="a-center movewishlist">
                  <?= $cart['count'] ?>
                </td>
                <td class="a-center movewishlist">
                  <span class="cart-price">
                    <span class="price">$<?= AppHelper::formatPrice($cart['count'] * ($item->price - $item->discount)) ?></span>
                  </span>
                </td>

              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <?= $this->render('_form',['model'=>$model]) ?>

      </div>


    </div>
  </div>
</section>