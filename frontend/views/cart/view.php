<?php
use frontend\helper\FrontEndHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\AppHelper;
$yourCart = FrontEndHelper::printCart();
?>
<section class="main-container col1-layout wow bounceInUp animated">
  <div class="main container">
    <div class="col-main">
      <form method="post" action="<?= Url::to(['cart/update-cart']) ?>">
        <div class="cart">
          <div class="page-title">
            <h2>Shopping Cart</h2>
          </div>
          <div class="table-responsive">

            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <fieldset>
              <table class="data-table cart-table" id="shopping-cart-table">
                <thead>
                <tr class="first last">
                  <th rowspan="1">&nbsp;</th>
                  <th rowspan="1"><span class="nobr">Tên sản phẩm</span></th>
                  <th colspan="1" class="a-center"><span class="nobr">Giá gốc(VND)</span></th>
                  <th rowspan="1" class="hidden-phone"><span class="nobr">Giảm giá(VND)</span></th>
                  <th class="a-center " rowspan="1">Số lượng</th>
                  <th class="a-center " rowspan="1">Size</th>
                  <th class="a-center " rowspan="1">Ghi chú</th>
                  <th colspan="1" class="a-center">Thành tiền(VND)</th>
                  <th class="a-center" rowspan="1">&nbsp;</th>
                </tr>
                </thead>
                <tfoot>
                <tr class="first last">
                  <td class="a-right last" colspan="9">
                    <button onclick="setLocation('<?= Url::to(['home/index']) ?>')" class="button btn-continue" title="Continue Shopping" type="button">
                      <span>Tiếp tục mua hàng</span>
                    </button>
                    <button onclick="setLocation('<?= Url::to(['home/index']) ?>')" class="button" title="Continue Shopping" type="submit">
                      <span>Cập nhật giỏ hàng</span>
                    </button>
                    <button onclick="window.location.href = baseUrl + '/cart/checkout'"  class="button" title="Clear Cart" type="button">
                      <span>Đặt hàng</span>
                    </button>
                    <button onclick="window.location.href = baseUrl + '/products/clear-cart'" id="empty_cart_button" class="button" title="Clear Cart" type="button">
                      <span>Hủy giỏ hàng</span>
                    </button>
                  </td>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($yourCart as $cart): ?>
                  <?php $item = $cart['item'];
                  $images = $item->images;
                  ?>
                  <tr class="first odd">
                    <td class="image">
                      <a href="<?= Url::to(['products/view', "id" => $item->id]) ?>" title="<?= $item->name ?>"
                         class="product-image">
                        <?= Html::img('@web/' . FrontEndHelper::imageUrl($images[0], $images[0]->file_path), [
                          'alt' => $item->name,
                          'width' => "75"
                        ]) ?>
                      </a>
                    </td>
                    <td><h2 class="product-name"><a
                          href="<?= Url::to(['products/view', "id" => $item->id]) ?>"><?= $item->name ?></a></h2></td>

                    <td class="a-center hidden-table price"data-value = "<?=$item->price?>" >
                      <a class="link-wishlist1 use-ajax" href="#"><?= AppHelper::formatPrice($item->price) ?></a>
                    </td>
                    <td class="a-center hidden-table discount" data-value = "<?=$item->discount?>">
                      <span class="cart-price">
                        <?= AppHelper::formatPrice($item->discount) ?>
                      </span>
                    </td>
                    <td class="a-center movewishlist">
                      <input maxlength="12" class="input-text qty" title="Qty" size="4" value="<?= $cart['count'] ?>" name="cart[<?= $item->id ?>][qty]">
                    </td>
                    <td class="a-center movewishlist">
                      <input maxlength="12" class="input-text qty" title="Size" size="4" value="<?= $cart['size'] ?>" name="cart[<?= $item->id ?>][size]">
                    </td>
                    <td class="a-center movewishlist">
                      <input maxlength="12" class="input-text qty" title="Qty" size="4" value="<?= $cart['note'] ?>" name="cart[<?= $item->id ?>][note]">
                    </td>
                    <td class="a-center movewishlist"><span class="cart-price"> <span
                          class="price">$<?= AppHelper::formatPrice($cart['count']*($item->price - $item->discount))?></span> </span></td>
                    <td class="a-center last">
                      <a class="button remove-item" title="Remove item" href="<?= Url::to(['products/remove-cart', "id" => $item->id]) ?>"><span><span>Remove item</span></span>
                      </a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </fieldset>
          </div>
          <!-- BEGIN CART COLLATERALS -->


          <!--cart-collaterals-->

        </div>

      </form>
    </div>
  </div>
</section>