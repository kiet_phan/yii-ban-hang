<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helper\FrontEndHelper;
use common\helpers\AppHelper;

?>
<div class="pdt-content is-loaded pdt_best_sales">
  <ul class="pdt-list products-grid-home zoomOut play">
    <?php foreach ($best_seller as $key => $item): ?>
      <?php
      $images = $item->images;
      if (count($images)):
        ?>
        <?php if ($key == 1) $class = "wide-first"; else $class = "" ?>
        <li class="item item-animate <?= $class ?>">
          <div class="item-inner">
            <div class="item-img">
              <div class="item-img-info">
                <a href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug]) ?>" title="<?= $item->name ?>"
                   class="product-image">
                  <?= Html::img('@web/' . FrontEndHelper::imageUrl($images[0], $images[0]->file_path), [
                    'alt' => $item->name,
                    'class' => ''
                  ]) ?>
                </a>
                <!--            <div class="new-label new-top-left">New</div>-->
                <div class="item-box-hover">
                  <div class="box-inner">
                    <div class="actions">
                      <div class="add_cart">
                        <button data-id="<?= $item->id ?>" class="button btn-cart" type="button">
                          <span>Add to Cart</span>
                        </button>
                      </div>
                      <div class="product-detail-bnt">
                        <a href="#" class="button detail-bnt"><span>Quick View</span></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item-info">
              <div class="info-inner">
                <div class="item-title"><a href="<?= Url::to(['san-pham/'.$item->id."-".$item->slug]) ?>"
                                           title="<?= $item->name ?>"><?= $item->name ?></a></div>
                <div class="item-content">
                  <div class="rating">
                    <div class="ratings">
                      <div class="rating-box">
                        <div class="rating" style="width:80%"></div>
                      </div>
                      <p class="rating-links"><a href="#">1 Review(s)</a> <span
                          class="separator">|</span> <a href="#">Add Review</a></p>
                    </div>
                  </div>
                  <div class="item-price">
                    <div class="price-box"><span class="regular-price"><span
                          class="price">VND <?= AppHelper::formatPrice($item->price) ?></span> </span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      <?php endif; ?>
    <?php endforeach; ?>

  </ul>
</div>