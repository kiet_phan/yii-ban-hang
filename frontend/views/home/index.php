<?php
?>
<!-- Slider -->
<?= \frontend\widget\SlideShowWidget::widget() ?>
<!-- end banner -->
<div class="main-col">
  <div class="container">
    <div class="row">
      <div class="product-grid-view">
        <div class="col-md-12">
          <div class="std">
            <div class="home-tabs wow bounceInUp animated">
              <div class="producttabs">
                <div id="magik_producttabs1" class="magik-producttabs">
                  <!--<h3></h3>-->
                  <div class="magik-pdt-container">
                    <!--Begin Tab Nav -->
                    <div class="magik-pdt-nav">
                      <ul class="pdt-nav">
                        <li class="item-nav tab-loaded tab-nav-actived" data-type="order" data-catid="" data-orderby="new_arrivals"
                            data-href="pdt_new_arrivals"><span class="title-navi">Hàng mới về</span></li>
                        <li class="item-nav " data-type="order" data-catid=""
                            data-orderby="best_sales" data-href="pdt_best_sales"><span
                            class="title-navi">Hàng bán chạy</span></li>

                      </ul>
                    </div>
                    <!-- End Tab Nav -->
                    <!--Begin Tab Content -->
                    <div class="magik-pdt-content wide-5">
                      <?= $this->render('_new_arrivals', [
                        'new_product'=>$new_product
                      ]) ?>
                      <?= $this->render('_best_seller', [
                        'best_seller'=>$best_seller
                      ]) ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Featured Slider -->

<!-- End Featured Slider -->