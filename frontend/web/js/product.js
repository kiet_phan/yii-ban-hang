/**
 * Created by phan-kiet on 4/27/16.
 */
jQuery(document).ready(function(){
  $('body').on('click','.btn-cart',function(event){
    productId = $(this).attr('data-id');
    var qty=$('#qty').val();
    var url = baseUrl + '/products/add-to-cart?id='+productId;
    if(qty != undefined && qty){
      url += "&qty="+qty
    }
    if(productId){
      window.location.href = url
    }
  });
  $('body').on('click','.create-account',function(event){
    window.location.href = baseUrl + '/customers/register';
  })
  $('body').on('change','#use_customer_info',function(event){
    if(this.checked){
      $.ajax({
        method: "GET",
        url: baseUrl + '/customers/get-user',
        dataType: "json",
        success: function(data){
          if(data && data.customer){
            var customer = data.customer
            $('[name="Transactions[first_name]"]').val(customer.first_name)
            $('[name="Transactions[last_name]"]').val(customer.last_name)
            $('[name="Transactions[province]"]').val(customer.province)
            $('[name="Transactions[district]"]').val(customer.district)
            $('[name="Transactions[user_email]"]').val(customer.email)
            $('[name="Transactions[user_phone]"]').val(customer.phone)
            var address = customer.ship_address || customer.address
            $('[name="Transactions[address]"]').val(address)
          }
        }
      });
    }
  });
  $('body').on('keyup','.input-text.qty',function(){
    $tr= $(this).closest('tr');
    var price = $tr.find('td.price').attr('data-value');
    var discount = $tr.find('td.discount').attr('data-value');
    discount = parseFloat(discount) > 0 ? parseFloat(discount) : 0;
    var qty = this.value;
    var totalPrice = parseInt(qty)*(parseFloat(price)- discount);
    //format(2, 3, '.', ',');  // "12.345.678,90"
    $tr.find('span.price').text(totalPrice.format(2,3,' ',','))
  })
});
Number.prototype.format = function(n, x, s, c) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
    num = this.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};